# HANDLEIDING #

Om DolVAV2 draaiend te krijgen zijn de volgende stappen nodig:

* Download Alpino en zet hem op een goede locatie neer. Denk aan de spaties, hier kan Alpino niet goed mee overweg
* Plaats afhankelijk van je OS runner.sh of runner.bat op een geschikte plaats en pas de paden in deze batch files aan aan jouw situatie.
    * OSX/Linux: kopiëer `runner.sh.example` naar `Alpino/bin` en verander de paden in het bestand. Hernoem het bestand naar `runner.sh`.
* Doen een GIT clone op het project
* Indien je een Linux gebruiker bent dien je de web/app/model/Settings.java aan te passen. Hier staan ook de paden in.
* Kopieër `conf/application.conf.example` naar `conf/application.conf` en pas de paden aan.
* Vervolgens kun je de dependencies met activator.bat of activator.sh inladen.
* Om activator te gebruiker voer je het commando "activator ui" uit
* Je kunt realtime de bestanden bewerken, maar ook met een IDE als Intellij of Eclipse.
* Installeer Neo4J (of gebruik in eerste instantie de externe Neo4J database)
* Configureer de plaats van Alpino en de server gegevens van Neo4J in application.conf
