package nl.hanze.model;

import java.util.ArrayList;
import java.util.Collection;

public class Question {
    private String question;
    private Collection<Answer> answers;

    public Question(String question) {
        this.question = question;
        this.answers = new ArrayList<Answer>();
    }

    public void addAnswer(String answer) {
        Answer answerobj = new Answer(answer, this);

        if(!this.answers.contains(answerobj)) {
            this.answers.add(answerobj);
        }
    }

    @Override
    public String toString() {
        return question;
    }
}
