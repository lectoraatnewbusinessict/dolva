package nl.hanze.model;

import sun.rmi.runtime.Log;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Marc on 16-3-2015.
 */
public class Logger {
    public static Logger _instance = null;
    private ArrayList<String> log;

    protected Logger() {
        this.log = new ArrayList<String>();
    }

    public static Logger getInstance() {
        if(_instance == null) {
            _instance = new Logger();
        }

        return _instance;
    }

    public void addToLog(String txt) {
        String msg = String.format("%tc - %s", new Date(), txt);

        log.add(msg);
        System.out.println(msg);
    }

    @Override
    public String toString() {
        StringWriter writer = new StringWriter();

        log.forEach((v) -> {
            writer.append(String.format("%s\n", v));
        });

        return writer.toString();
    }
}
