package nl.hanze;

import nl.hanze.model.Logger;
import nl.hanze.test.DolvaTest;
import nl.hanze.test.Test;
import nl.hanze.test.VirtualTherapistTest;

public class Main {

    public static void main(String[] args) {
        Logger.getInstance().addToLog("---------------------------------------------------");
        Logger.getInstance().addToLog("VirtualTherapist test");

        Test test = new VirtualTherapistTest();
        test.start();

        Logger.getInstance().addToLog("---------------------------------------------------");
        Logger.getInstance().addToLog("DolVa test");

        Test test2 = new DolvaTest();
        test2.start();

        Logger.getInstance().addToLog("---------------------------------------------------");
        Logger.getInstance().addToLog("Klaar met testen");
        Logger.getInstance().addToLog("---------------------------------------------------");
    }
}
