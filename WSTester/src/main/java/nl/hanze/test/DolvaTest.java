package nl.hanze.test;


import nl.hanze.model.Logger;
import nl.hanze.model.Question;
import nl.hanze.webservice.dolva.DolvaWebservice;
import nl.hanze.webservice.virtualtherapist.VirtualTherapistWebservice;

import java.io.IOException;

public class DolvaTest extends Test implements ITest {
    private DolvaWebservice ws;
    private String username = "test@test.nl";
    private String password = "test123";
    private String url = "http://dolva.arankadol.nl:8080/api";

    public DolvaTest() {
        this.ws = new DolvaWebservice(url, null, username, password);
        this.authenticate();
    }

    @Override
    public void authenticate() {
        super.authenticate();

        try {
            if (!this.ws.authenticate(username, password)) {
                Logger.getInstance().addToLog("Inloggengegevens zijn niet geldig");
            }

            Logger.getInstance().addToLog("Inloggen succesvol");
        } catch (Exception ex) {
            Logger.getInstance().addToLog(ex.getMessage());
        }
    }

    @Override
    public void sendQuestion(Question question) {
        Logger.getInstance().addToLog("Vraag versturen: \"" + question + "\"");

        try {
            this.ws.sendQuestion(question.toString());
        } catch (IOException e) {
            Logger.getInstance().addToLog(e.getMessage());
        }
    }
}
