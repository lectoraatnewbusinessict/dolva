package nl.hanze.test;


import nl.hanze.model.Question;

public interface ITest {
    void authenticate();
    void sendQuestion(Question question);
}
