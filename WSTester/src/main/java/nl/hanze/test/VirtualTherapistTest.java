package nl.hanze.test;


import nl.hanze.model.Logger;
import nl.hanze.model.Question;
import nl.hanze.webservice.HTTPResponse;
import nl.hanze.webservice.virtualtherapist.VirtualTherapistWebservice;
import nl.hanze.webservice.virtualtherapist.VirtualTherapistWebsocket;

import javax.json.Json;
import javax.json.JsonObject;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;

public class VirtualTherapistTest extends Test implements ITest {
    private VirtualTherapistWebservice ws;
    private String username = "test@test.nl";
    private String password = "test123";
    private String url = "http://127.0.0.1:9000/api";
    private String urlws = "ws://127.0.0.1:9000/server";
    private final VirtualTherapistWebsocket clientEndPoint = new VirtualTherapistWebsocket(URI.create(urlws));
    private volatile boolean gofurther;

    public VirtualTherapistTest() {
        this.ws = new VirtualTherapistWebservice(url, urlws, username, password);

        clientEndPoint.addMessageHandler(message -> {
            Logger.getInstance().addToLog("Antwoord ontvangen: " + message);
            gofurther = true;
        });
    }

    @Override
    public void authenticate() {
        super.authenticate();

        try {
            if(!this.ws.authenticate(username, password)) {
                Logger.getInstance().addToLog("Inloggengegevens zijn niet geldig");
            }

            Logger.getInstance().addToLog("Inloggen succesvol");
        } catch (IOException e) {
            //e.printStackTrace();
            Logger.getInstance().addToLog(e.getMessage());
        }
    }

    @Override
    public void sendQuestion(Question question) {
        //  this.ws.sendQuestion(question.toString());

        try {
            clientEndPoint.sendQuestion(
                    question.toString(),
                    this.ws.getAuthHeaderValue(),
                    "1",
                    this.username
            );

            while (true) {
                if(gofurther) {
                    gofurther = false;
                    break;
                }
            }

        } catch (Exception e) {
            //e.printStackTrace();
            Logger.getInstance().addToLog(e.getMessage());
        }
    }
}
