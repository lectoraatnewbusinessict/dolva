package nl.hanze.test;

import nl.hanze.model.Logger;
import nl.hanze.model.Question;

import java.util.ArrayList;
import java.util.Arrays;

public abstract class Test implements ITest {
    private ArrayList<Question> questions;

    public Test() {
        questions = new ArrayList<Question>();

        addQuestion("Is de aarde rond?", new ArrayList<String>(
                Arrays.asList("Ja", "De aarde is inderdaad rond")
        ));

        addQuestion("Hebben we een ronde wereld?", new ArrayList<String>(
                Arrays.asList("Inderdaad", "De wereld niet, maar de aarde wel.")
        ));

        addQuestion("Is de aarde rond?", new ArrayList<String>(
                Arrays.asList("Ja", "De aarde is inderdaad rond", "Geen idee")
        ));
    }

    @Override
    public void authenticate() {
        Logger.getInstance().addToLog("---------------------------------------------------");
        Logger.getInstance().addToLog("Authenticatie");
        Logger.getInstance().addToLog("---------------------------------------------------");
    }

    public void start() {
        this.authenticate();
        Logger.getInstance().addToLog("---------------------------------------------------");
        Logger.getInstance().addToLog("Vragen stellen");
        Logger.getInstance().addToLog("---------------------------------------------------");
        this.questions.forEach(this::sendQuestion);
    }

    private void addQuestion(String question, ArrayList<String> answers) {
        Question q = new Question(question);
        answers.forEach(q::addAnswer);
        this.questions.add(q);
    }
}
