package nl.hanze.webservice.dolva;

import nl.hanze.webservice.HTTPResponse;
import nl.hanze.webservice.IWebservice;
import nl.hanze.webservice.Webservice;

import javax.json.JsonObject;
import java.io.IOException;
import java.util.HashMap;

public class DolvaWebservice extends Webservice implements IWebservice {
    private String token;

    public DolvaWebservice(String url, String wsUrl, String username, String password) {
        super(url);
        this.setWSURL(wsUrl);
        this.setUsername(username);
        this.setPassword(password);
    }

    @Override
    public boolean authenticate(String username, String password) throws IOException {
        HashMap<String, String> headers = new HashMap<String, String>();

        HTTPResponse response = this.post("users/login?email=a.dol@pl.hanze.nl&password=Paragr@@f", headers, "");

        if(response.getJson() != null) {
            this.token = response.getJson().get("auth_token").toString();
        }

        return response.getCode() == 200;
    }

    @Override
    public void sendQuestion(String question) throws IOException {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Basic " + token);

        HTTPResponse response = this.get("answers/question?question=" + question, headers, "");
    }
}
