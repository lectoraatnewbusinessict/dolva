package nl.hanze.webservice;

import java.io.IOException;

public interface IWebservice {
    boolean authenticate(String username, String password) throws IOException;
    void sendQuestion(String question) throws IOException;
}
