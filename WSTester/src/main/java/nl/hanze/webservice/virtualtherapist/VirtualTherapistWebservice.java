package nl.hanze.webservice.virtualtherapist;

import nl.hanze.model.Logger;
import nl.hanze.utils.HashUtil;
import nl.hanze.webservice.HTTPResponse;
import nl.hanze.webservice.IWebservice;
import nl.hanze.webservice.Webservice;

import java.io.IOException;
import java.util.HashMap;

public class VirtualTherapistWebservice extends Webservice implements IWebservice {

    public VirtualTherapistWebservice(String url, String wsUrl, String username, String password) {
        super(url);
        this.setWSURL(wsUrl);
        this.setUsername(username);
        this.setPassword(password);
    }

    @Override
    public boolean authenticate(String username, String password) throws IOException {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authentication", getAuthHeaderValue());

        HTTPResponse response = this.get("login", headers, "");
        return response.getCode() == 200;
    }

    @Override
    public void sendQuestion(String question) throws IOException {
        // Headers voor authenticatie bouwen
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("authentication", getAuthHeaderValue());

        // Posten van data
        String postdata = "mood=Vrolijk";
        Logger.getInstance().addToLog("Vraag versturen: \"" + question + "\"");

        // Context starten
        //HTTPResponse response = this.post("context", headers, postdata);
    }

    public String getAuthHeaderValue() {
        String hash = HashUtil.createHash(getUsername(), getPassword());
        Logger.getInstance().addToLog("Authenticatie header: " + hash);
        return hash;
    }
}
