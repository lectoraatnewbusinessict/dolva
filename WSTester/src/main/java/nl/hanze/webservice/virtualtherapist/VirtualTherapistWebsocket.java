package nl.hanze.webservice.virtualtherapist;

import nl.hanze.model.Logger;
import org.json.JSONObject;

import javax.websocket.*;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;


@ClientEndpoint
public class VirtualTherapistWebsocket {
    private Session userSession = null;
    private MessageHandler messageHandler;

    public VirtualTherapistWebsocket(URI url) {
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, url);
        } catch (Exception e) {
            Logger.getInstance().addToLog(e.getMessage());
        }
    }

    @OnOpen
    public void onOpen(Session userSession) {
        this.userSession = userSession;
    }

    @OnClose
    public void onClose(Session userSession, CloseReason reason) {
        this.userSession = null;
    }

    @OnMessage
    public void onMessage(String message) {
        if (this.messageHandler != null)
            this.messageHandler.handleMessage(message);
    }

    public void addMessageHandler(MessageHandler msgHandler) {
        this.messageHandler = msgHandler;
    }

    public void sendQuestion(String question, String authToken, String chatid, String email) {
        JSONObject jsonObj = new JSONObject();

        jsonObj.put("token",     authToken);
        jsonObj.put("question",  question);
        jsonObj.put("chatid",    chatid);
        jsonObj.put("email",     email);

        Logger.getInstance().addToLog("Sending question: " + jsonObj.toString());

        this.userSession.getAsyncRemote().sendText(jsonObj.toString());
    }

    public static interface MessageHandler {
        public void handleMessage(String message);
    }
}
