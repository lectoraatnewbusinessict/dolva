package nl.hanze.webservice;

import nl.hanze.model.Logger;
import nl.hanze.utils.HashUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public abstract class Webservice implements IWebservice {
    protected String url;
    private String username;
    private String password;
    private String wsurl;

    public Webservice(String url) {
        this.setURL(url);
    }

    public HTTPResponse get(String resource, HashMap<String, String> headers, String content) throws IOException {
        HttpURLConnection httpConn = getConn(resource, headers, "GET");

        if(content != null && content != "") {
            writeToHTTPOutputStream(httpConn, content);
        }

        return getHTTPResponse(httpConn);
    }

    public HTTPResponse post(String resource, HashMap<String, String> headers, String content) throws IOException {
        HttpURLConnection httpConn = getConn(resource, headers, "POST");

        if(content != null && content != "") {
            httpConn.setDoOutput(true);
            writeToHTTPOutputStream(httpConn, content);
        }

        return getHTTPResponse(httpConn);
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getWsurl() {
        return wsurl;
    }

    public String getURL() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setWSURL(String wsurl) {
        this.wsurl = wsurl;
    }

    public URI getURI() {
        return URI.create(this.getURL());
    }

    public void setURL(String url) {
        this.url = url;
    }

    protected HttpURLConnection getConn(String resource, HashMap<String, String> headers, String type) throws IOException {
        URL url = new URL(String.format("%s/%s", this.getURL(), resource));
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setRequestMethod(type);

        Logger.getInstance().addToLog(String.format("%s: %s", type, url.toString()));

        // Headers
        if(headers != null && headers.size() > 0) {
            headers.forEach(httpConn::setRequestProperty);
        }

        // Voor de GET (response)
        httpConn.setRequestProperty("User-Agent", "Mozilla/5.0 (compatible)");
        httpConn.setRequestProperty("Accept", "*/*");

        return  httpConn;
    }

    protected void writeToHTTPOutputStream(HttpURLConnection httpConn, String content) throws IOException{
        OutputStreamWriter out = new OutputStreamWriter(httpConn.getOutputStream());
        out.write(content);
        out.close();
    }

    protected HTTPResponse getHTTPResponse(HttpURLConnection httpConn) throws IOException {
        HTTPResponse response = new HTTPResponse(httpConn.getResponseCode());
        String inputStreamString = new Scanner(httpConn.getInputStream(),"UTF-8").useDelimiter("\\A").next();

        response.setContent(inputStreamString);

        Logger.getInstance().addToLog("Antwoord: " + response.getCode() + " (" + httpConn.getResponseMessage() + ")");
        return response;
    }

    protected void setUsername(String username) {
        this.username = username;
    }

    protected void setPassword(String password) {
        this.password = password;
    }

}
