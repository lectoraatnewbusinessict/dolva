package nl.hanze.webservice;

import jdk.nashorn.internal.parser.JSONParser;
import nl.hanze.model.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParser;
import java.io.StringReader;

public class HTTPResponse {
    private int code;
    private String content;
    private JsonObject json = null;

    public HTTPResponse(int code) {
        this.code = code;
        this.content = content;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;

        try {
            JsonReader json = Json.createReader(new StringReader(content));
            this.json = json.readObject();
        } catch (Exception ex) {
            Logger.getInstance().addToLog("JSON data is ongeldig");
        }
    }

    public JsonObject getJson() {
        return json;
    }
}
