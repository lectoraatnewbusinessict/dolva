import com.typesafe.sbt.SbtNativePackager._
import NativePackagerKeys._

maintainer in Linux := "First Lastname <first.last@example.com>"

packageSummary in Linux := "My custom package summary"

packageDescription := "My longer package description"

name := """DolVa"""

version := "1.1"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

resolvers ++= Seq(
  "neo4j-releases" at "http://m2.neo4j.org/releases/"
)

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  javaCore,
  javaEbean,
  javaWs,
  "org.neo4j" % "neo4j" % "2.2.1"
)

libraryDependencies ++= Seq(
  "org.powermock" % "powermock-core" % "1.5.6" % Test,
  "org.powermock" % "powermock-module-junit4" % "1.5.6" % Test,
  "org.powermock" % "powermock-api-mockito" % "1.5.6" % Test
)
fork in run := true