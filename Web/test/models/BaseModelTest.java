package models;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.support.membermodification.MemberModifier;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import play.libs.Json;
import services.GraphDBService;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(GraphDBService.class)
public class BaseModelTest {
    private BaseModel testModel;

    @Before
    public void beforeEach() throws Exception {
        this.testModel = new BaseModel() {
            @Override
            public String getType() {
                return "TestModel";
            }
        };
    }

    @Test
    public void testCreate() throws Exception {
        mockStatic(GraphDBService.class);

        BaseModel spyModel = spy(this.testModel);

        Map<String, String> properties = new HashMap<>();
        properties.put("Key", "Value");
        properties.put("Putputt", "Pepper");
        when(spyModel.getProperties()).thenReturn(properties);

        ObjectNode fakeJsonNode = Json.newObject();
        fakeJsonNode.put("id", 0);
        when(GraphDBService.execute(eq("CREATE (model:TestModel{Putputt: \"Pepper\", Key: \"Value\"}) RETURN model")))
                .thenReturn(fakeJsonNode);

        spyModel.create();
        assertEquals(spyModel.getId(), 0);
    }

    @Test
    public void testAddProperty() throws Exception {
        this.testModel.addProperty("testKey", "testValue");
        Map<String, String> properties = this.testModel.getProperties();
        assertTrue("Property not added!", properties.containsKey("testKey"));
    }

    @Test
    public void testAddProperties() throws Exception {
        // Create a map of properties and add it to the test model.
        Map<String, String> properties = new HashMap<>();
        properties.put("testKey1", "testValue1");
        properties.put("testKey2", "testValue2");
        this.testModel.addProperties(properties);

        // Retrieve the model properties.
        properties = this.testModel.getProperties();

        // Check if the properties contain the added properties.
        assertTrue("Property 1 not added!", properties.containsKey("testKey1"));
        assertTrue("Property 2 not added!", properties.containsKey("testKey2"));
    }

    @Test
    public void testRelateTo() throws Exception {
        mockStatic(GraphDBService.class);

        MemberModifier.field(BaseModel.class, "id").set(this.testModel, 1);
        this.testModel.relateTo(2, "ABOUT");

        verifyStatic(times(1));
        GraphDBService.execute("MATCH (n), (m) WHERE ID(n) = 1 AND ID(m) = 2 CREATE (n)-[:ABOUT]->(m)");
    }

    @Test
    public void testGetId() throws Exception {
        MemberModifier.field(BaseModel.class, "id").set(this.testModel, 1);
        assertEquals(this.testModel.getId(), 1);
    }
}
