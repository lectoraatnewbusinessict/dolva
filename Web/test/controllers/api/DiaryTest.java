package controllers.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.api.parser.diary.FeelingsParser;
import controllers.api.parser.diary.IrrelevantBehaviorParser;
import controllers.api.parser.diary.ObstaclesParser;
import models.diary.DiaryMetaModel;
import models.diary.DiaryModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import play.libs.Json;
import play.mvc.Result;
import play.test.FakeRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.routeAndCall;

@RunWith(PowerMockRunner.class)
@PrepareForTest({
        DiaryController.class,
        FeelingsParser.class,
        ObstaclesParser.class,
        IrrelevantBehaviorParser.class
})
public class DiaryTest {

    private DiaryMetaModel mockMetaModel;
    private DiaryModel mockModel ;

    @Before
    public void before() throws Exception {
        this.mockMetaModel = mock(DiaryMetaModel.class);
        this.mockModel = mock(DiaryModel.class);

        whenNew(DiaryMetaModel.class).withAnyArguments().thenReturn(this.mockMetaModel);
        whenNew(DiaryModel.class).withNoArguments().thenReturn(this.mockModel);
    }

    @Test
    public void testPostChainValid() throws Exception {
        ObjectNode json = Json.newObject();
        json.put("name", "Kevin Bateman");
        json.put("date", 1444395600L);

        json.putObject("feelings").put("question", "Hoe gaat het vandaag met je?").put("answer", "Kon minder");

        ArrayNode obstacleAnswer = json.putObject("obstacles").put("question", "Was er vandaag sprake van:").putArray("answer");
        obstacleAnswer.addObject().put("text", "een eetbui of snaaiaanval").put("intensity", "intens maar het duurde kort");
        obstacleAnswer.addObject().put("text", "eten zonder bewuste aandacht").put("intensity", "licht en het duurde kort");

        json.putObject("irrelevantBehavior")
                .put("question", "Was er sprake van zgn. irrelevant gedrag?")
                .put("answer", "Ja")
                .put("explanation", "Het was geen goede dag vandaag");

        Result r = routeAndCall(new FakeRequest("POST", "/api/diary").withJsonBody(json), 10000);
        JsonNode response = new ObjectMapper().readTree(contentAsString(r));

        assertEquals(200, r.toScala().header().status());
        assertTrue(response.isObject());
        assertEquals(this.mockMetaModel.getId(), response.findPath("id").asInt());

        verify(this.mockMetaModel, times(1)).create();
        verify(this.mockModel, atLeast(3)).create();

        int mockModelID = this.mockModel.getId();
        verify(this.mockMetaModel, times(3)).relateTo(mockModelID, "ENTRY");
    }

    @Test
    public void testPostChainNoJSON() throws Exception {
        Result r = routeAndCall(new FakeRequest("POST", "/api/diary"), 10000);
        JsonNode response = new ObjectMapper().readTree(contentAsString(r));

        assertEquals(400, r.toScala().header().status());
        assertEquals("No JSON data found", response.findPath("error").asText());
    }

    @Test
    public void testPostChainMissingData() throws Exception {
        ObjectNode json = Json.newObject();
        json.put("name", "Kevin Bateman");
        json.put("date", 1444395600L);

        Result r = routeAndCall(new FakeRequest("POST", "/api/diary").withJsonBody(json), 10000);
        JsonNode response = new ObjectMapper().readTree(contentAsString(r));

        assertEquals(400, r.toScala().header().status());
        assertTrue(response.findPath("error").asText().contains("Sent JSON object is missing the following keys"));
        verify(this.mockMetaModel, times(1)).deleteAllFromHere();
    }

}
