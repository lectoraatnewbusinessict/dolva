package controllers.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.api.parser.chain.*;
import models.chain.ChainAnalysisMetaModel;
import models.chain.ChainAnalysisModel;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import play.libs.Json;
import play.mvc.Result;
import play.test.FakeRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.routeAndCall;

@RunWith(PowerMockRunner.class)
@PrepareForTest({
        ChainController.class,
        ProblemBehaviorParser.class,
        ProvocationParser.class,
        VulnerabilityFactorsParser.class,
        EventChainParser.class,
        ConsequencesParser.class,
        PreventionStrategyParser.class
})
public class ChainAnalysisTest {

    private ChainAnalysisMetaModel mockMetaModel;
    private ChainAnalysisModel mockModel;

    @Before
    public void before() throws Exception {
        this.mockMetaModel = mock(ChainAnalysisMetaModel.class);
        this.mockModel = mock(ChainAnalysisModel.class);

        whenNew(ChainAnalysisMetaModel.class).withAnyArguments().thenReturn(this.mockMetaModel);
        whenNew(ChainAnalysisModel.class).withNoArguments().thenReturn(this.mockModel);
    }

    @Test
    public void testPostChainValid() throws Exception {
        ObjectNode json = Json.newObject();
        json.put("name", "Kevin Bateman");
        json.put("analysisDate", 1444582383L);
        json.put("behaviorDate", 1444395600L);

        ObjectNode problemBehavior = json.putObject("problemBehavior");
        problemBehavior.put("question", "Ik had een...");
        problemBehavior.putArray("answer").add("eetaanval, eetbui").add("klein, maar wel objectief: snaaiaanval");
        problemBehavior.putArray("food").add("ijs").add("snoep").add("brood");

        ObjectNode provocation = json.putObject("provocation");
        provocation.put("question", "Welke gebeurtenis vond er plaats? Wanneer begon het? Wat was er aan de hand?");
        provocation.put("answer", "Ik kwam net thuis van een slechte dag op school, en begon meteen de koelkast leeg te eten.");

        ObjectNode vulnerabilityFactors = json.putObject("vulnerabilityFactors");
        vulnerabilityFactors.put("question", "Welke van deze factoren maakten je kwetsbaar voor de uitlokkende gebeurtenis?");
        ArrayNode vulnerabilityFactorsAnswer = vulnerabilityFactors.putArray("answer");
        ArrayNode factorsArray = vulnerabilityFactorsAnswer.addObject().put("category", "gerelateerd met voeding").putArray("factors");
        factorsArray.addObject().put("text", "ik was op dieet").put("importance", 4);
        factorsArray.addObject().put("text", "ik had nog niet gegeten").put("importance", 2);

        ObjectNode eventChain = json.putObject("eventChain");
        eventChain.put("question", "Hoe verliep het proces tussen aanleiding en probleemgedrag precies?");
        ArrayNode eventChainAnswer = eventChain.putArray("answer");
        eventChainAnswer.addObject().put("type", "activity").put("text", "ik was tv aan het kijken");
        eventChainAnswer.addObject().put("type", "event").put("text", "mijn ouders kregen ruzie met elkaar");
        eventChainAnswer.addObject().put("type", "sensation").put("text", "gevoel van uitputting");
        eventChainAnswer.addObject().put("type", "feeling").put("text", "machteloos");
        eventChainAnswer.addObject().put("type", "cognition").put("text", "ik ben schuldig");

        ObjectNode consequences = json.putObject("consequences");
        consequences.put("question", "Wat waren de directe gevolgen van je gedrag?");
        consequences.putArray("answer").addObject()
                .put("text", "De koelkast is door mij leeggegeten.")
                .put("repair", "Ik ga voor mijn ouders naar de supermarkt om te helpen de koelkast weer te vullen.");

        ObjectNode preventionStrategy = json.putObject("preventionStrategy");
        preventionStrategy.put("question", "De volgende keer dat me dit overkomt, ga ik:");
        preventionStrategy.put("answer", "mijn hulplijn bellen");

        Result r = routeAndCall(new FakeRequest("POST", "/api/chain").withJsonBody(json), 10000);
        JsonNode response = Json.parse(contentAsString(r));

        assertEquals(200, r.toScala().header().status());
        assertTrue(response.isObject());
        assertEquals(this.mockMetaModel.getId(), response.findPath("id").asInt());

        verify(this.mockMetaModel, times(1)).create();
        verify(this.mockModel, atLeast(6)).create();

        int mockModelID = this.mockModel.getId();
        verify(this.mockMetaModel, times(6)).relateTo(mockModelID, "STEP");
    }

    @Test
    public void testPostChainNoJSON() throws Exception {
        Result r = routeAndCall(new FakeRequest("POST", "/api/chain"), 10000);
        JsonNode response = Json.parse(contentAsString(r));

        assertEquals(400, r.toScala().header().status());
        assertEquals("No JSON data found", response.findPath("error").asText());
    }

    @Test
    public void testPostChainMissingData() throws Exception {
        ObjectNode json = Json.newObject();
        json.put("name", "Kevin Bateman");
        json.put("analysisDate", 1444582383L);
        json.put("behaviorDate", 1444395600L);

        Result r = routeAndCall(new FakeRequest("POST", "/api/chain").withJsonBody(json), 10000);
        JsonNode response = Json.parse(contentAsString(r));

        assertEquals(400, r.toScala().header().status());
        assertTrue(response.findPath("error").asText().contains("Sent JSON object is missing the following keys"));
        verify(this.mockMetaModel, times(1)).deleteAllFromHere();
    }

}
