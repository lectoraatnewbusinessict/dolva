package forms;

import play.data.validation.Constraints;

public class QuestionForm {

    @Constraints.Required
    public String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
