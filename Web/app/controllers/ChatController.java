package controllers;

import play.mvc.Controller;
import play.mvc.Result;
import services.QuestionService;

public class ChatController extends Controller {
    public static Result index() {
        QuestionService service = new QuestionService();
        return ok(views.html.ChatController.index.render());
    }
}
