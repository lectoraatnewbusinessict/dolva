package controllers;

import forms.QuestionForm;
import models.Answer;
import models.NormalisedText;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import services.LogService;
import services.QuestionService;
import services.parser.Alpino;
import services.parser.DependencyParser;
import services.TestService;
import models.User;
import services.UserService;
import views.UserController.Index;
//import views.TestController.Index; 

import java.util.ArrayList;

public class UserController extends Controller {

	private final static UserService userService = new UserService();

	public static Result index() {
		ArrayList<User> users = userService.getUsers();

		Index index = new Index();
		index.setUsers(users);

		return ok(views.html.UserController.index.render(index));
	}

	public static Result add() {
		return ok(views.html.UserController.add.render());
	}

	public static Result edit(long id) {
		// String strId = Long.toString(id);
		ArrayList<User> users = userService.getUser(id);

		Index index = new Index();
		index.setUsers(users);

		return ok(views.html.UserController.edit.render(index));

	}

	public static Result submit() {
		DynamicForm requestData = Form.form().bindFromRequest();
		DependencyParser parser = new Alpino();
		//
		try {
			ArrayList<NormalisedText> users = new ArrayList<>();
			for (String key : request().body().asFormUrlEncoded().keySet()) {
				parser.parseText(requestData.data().get(key));
				users.add(parser.getOutPut());

			}
			userService.saveUsersToDB(users);
			// return ok(views.html.UserController.add.render());
		} catch (Exception ex) {
			LogService.getInstance().error(ex.getMessage());
			ex.printStackTrace();
			// return badRequest(ex.getMessage());
			// return ok(views.html.UserController.add.render());
		}
		// return redirect(routes.TestController.index());
		return redirect(routes.UserController.index());
	}

	public static Result delete(long id) {
		ArrayList<User> users = userService.getUser(id);

		Index index = new Index();
		index.setUsers(users);

		return ok(views.html.UserController.delete.render(index));
	}

	public static Result removeUser(long id) {
		try {
			userService.deleteUserFromDB(id);
		} catch (Exception ex) {
			LogService.getInstance().error(ex.getMessage());
			ex.printStackTrace();
		}
		return redirect(routes.UserController.index());
	}

	public static Result change(long id) {
		// Hier moet het id worden meegegeven (zelfde manier als bij edit()),
		// zodat de informatie van dat antwoord wordt aangepast
		// Ook moet hierin de informatie uit de pagina worden uitgelezen
		// Deze informatie moet worden opgeslagen in een ArrayList net als bij
		// submit()
		// NIET VERGETEN: (id:Long) zetten bij routes
		 DynamicForm requestData = Form.form().bindFromRequest();
		// Form<QuestionForm> filledForm = questionForm.bindFromRequest();
		 DependencyParser parser = new Alpino();
		 try {
		 ArrayList<NormalisedText> users = new ArrayList<>();
		//
		// // QuestionForm form = new QuestionForm();
		 for (String key : request().body().asFormUrlEncoded().keySet()) {
		// if (!key.startsWith("answer")
		// || requestData.data().get(key).equals(""))
		// continue;
		 parser.parseText(requestData.data().get(key));
		 users.add(parser.getOutPut());
		 }
		// // return ok(views.html.QuestionController.test.render());
		 userService.changeUserInDB(id,users);
		}
		//
		 catch (Exception ex) {
		 LogService.getInstance().error(ex.getMessage());
		 ex.printStackTrace();
		 }
		return redirect(routes.UserController.index());
	}

}
