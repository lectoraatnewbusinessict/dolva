package controllers;

import forms.QuestionForm;
import models.Answer;
import models.NormalisedText;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import services.LogService;
import services.QuestionService;
import services.parser.Alpino;
import services.parser.DependencyParser;
import views.QuestionController.Index;

import java.util.ArrayList;

public class QuestionController extends Controller {
	private final static Form<QuestionForm> questionForm = Form
			.form(QuestionForm.class);
	private final static QuestionService questionService = new QuestionService();
	private final static boolean permission = Application.isAdmin();

	public static Result index() {
		if(permission == true){
			ArrayList<Answer> answers = questionService.getAnswers();
	
			Index index = new Index();
			index.setAnswers(answers);

			return ok(views.html.QuestionController.index.render(index));
		}else{
			return Application.redirect();
		}
	}
	
	public static Result welcome(){
		return ok(views.html.QuestionController.welcome.render());
	}

	public static Result edit(long id) {
		// String strId = Long.toString(id);
		ArrayList<Answer> answers = questionService.getAnswer(id);

		Index index = new Index();
		index.setAnswers(answers);

		return ok(views.html.QuestionController.edit.render(index));

	}
	
	public static Result delete(long id){
		
		ArrayList<Answer> answers = questionService.getAnswer(id);
		
		Index index = new Index();
		index.setAnswers(answers);
		
		return ok(views.html.QuestionController.delete.render(index));
	}
	
	public static Result removeAnswer(long id){
		try {
			questionService.DeleteAnswerFormDB(id);
		}
		catch(Exception ex) {
			LogService.getInstance().error(ex.getMessage());
			ex.printStackTrace();
		}
		return redirect(routes.QuestionController.index());
	}

	// Klasse om te kijken of de methode changeAnswersInDB() wordt
	// aangeroepen
	public static Result submitChange(long id) {
		// Hier moet het id worden meegegeven (zelfde manier als bij edit()),
		// zodat de informatie van dat antwoord wordt aangepast
		// Ook moet hierin de informatie uit de pagina worden uitgelezen
		// Deze informatie moet worden opgeslagen in een ArrayList net als bij
		// submit()
		// NIET VERGETEN: (id:Long) zetten bij routes
		DynamicForm requestData = Form.form().bindFromRequest();
		Form<QuestionForm> filledForm = questionForm.bindFromRequest();
		DependencyParser parser = new Alpino();
		try {
			ArrayList<NormalisedText> answers = new ArrayList<>();

			// QuestionForm form = new QuestionForm();
			for (String key : request().body().asFormUrlEncoded().keySet()) {
				if (!key.startsWith("answer")
						|| requestData.data().get(key).equals(""))
					continue;
				parser.parseText(requestData.data().get(key));
				answers.add(parser.getOutPut());
			}
			// return ok(views.html.QuestionController.test.render());
			questionService.changeAnswersInDB(id, filledForm.get().getCategory(), answers);
		}

		catch (Exception ex) {
			LogService.getInstance().error(ex.getMessage());
			ex.printStackTrace();
		}
		return redirect(routes.QuestionController.index());
	}

	public static Result add() {
		return ok(views.html.QuestionController.add.render(questionForm));
	}

	public static Result submit() {
		DynamicForm requestData = Form.form().bindFromRequest();
		Form<QuestionForm> filledForm = questionForm.bindFromRequest();
		DependencyParser parser = new Alpino();

		if (filledForm.hasErrors()) {
			return ok(views.html.QuestionController.add.render(filledForm));
		}

		try {
			ArrayList<NormalisedText> answers = new ArrayList<>();
			for (String key : request().body().asFormUrlEncoded().keySet()) {
				if (!key.startsWith("answer")
						|| requestData.data().get(key).equals(""))
					continue;
				parser.parseText(requestData.data().get(key));
				answers.add(parser.getOutPut());
			}

			questionService.saveAnswersToDB(filledForm.get().getCategory(),
					answers);
		} catch (Exception ex) {
			LogService.getInstance().error(ex.getMessage());
			ex.printStackTrace();
			// return badRequest(ex.getMessage());
		}

		return redirect(routes.QuestionController.index());
	}
}
