package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

import models.Account;

import services.ControlPanelService;

import java.util.ArrayList;

public class Application extends Controller {

	public static Result index() {
		return ok(index.render("Your new application is ready."));
	}

	public static Result getUsername(String temp) {
		// todo: Perhaps add a list of unwanted usernames (such as homophobic,
		// anti-religious & racial slurs.)
		String exists;
		ControlPanelService service = new ControlPanelService();
		ArrayList<Account> accounts = service.getAccount(temp.toLowerCase());
		if (accounts.size() > 0) {
			exists = temp + " is niet meer sdfsdf beschikbaar.";
		} else {
			exists = temp + " is beschikbaar.";
		}
		// else { exists = "Deze username is niet toegestaan."; }
		return ok(ajax_result.render(exists));
	}
	
	public static Result redirect(){
		return redirect(routes.AuthenticationController.index());
	}

	public static Result comparePW(String pw1, String pw2) {
		String temp;
		System.out.println(pw1 + pw2);
		if (!pw2.equals(pw1)) {
			temp = "Wachtwoord komt niet overeen.";
		} else {
			temp = "Wachtwoord komt overeen.";
		}
		return ok(ajax_result.render(temp));
	}

	public static Result javascriptRoutes() {
		response().setContentType("text/javascript");
		return ok(Routes.javascriptRouter("jsRoutes",
				// Routes

		controllers.routes.javascript.Application.getUsername()));
	}

	/*
	 * public static boolean isRole() {
	 * String role = session("role"); 			// This retrieves the string stored in the session("role")
	 * boolean temp = false; 					// Set the default return false
	 * if ("Role".equals(session("role"))) {
	 * 		temp = true; 						// if the role is equal to "Role" it will set temp as true.
	 * }
	 * return temp; 							// returns either true or false.
	 */
	
	public static boolean isAdmin() {
		String role = session("role");
		boolean temp = false;
		if ("Admin".equals(session("role"))) {
			temp = true;
		}
		return temp;
	}
	public static boolean isModerator() {
		String role = session("role");
		boolean temp = false;
		if ("Moderator".equals(session("role"))) {
			temp = true;
		}
		return temp;
	}
	
	public static boolean isUser() {
		String role = session("role");
		boolean temp = false;
		if ("User".equals(session("role"))) {
			temp = true;
		}
		return temp;
	}
	
	public static boolean isConnected() {
		String user = session("connected");
		boolean temp = false;
		if(!"".equals(user) || !user.isEmpty()){
			temp = true;
		}
		return temp;
	}
	
}