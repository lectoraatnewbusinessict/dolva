package controllers;

import models.Answer;
import models.NormalisedText;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import services.LogService;
import services.QuestionService;
import services.parser.Alpino;
import services.parser.DependencyParser;
import views.CaloriesController.Index;
import services.CaloriesService;
import models.Product;
//import views.TestController.Index; 

import java.util.ArrayList;

public class CaloriesController extends Controller {
     // private final static Form<QuestionForm> questionForm = Form.form(QuestionForm.class);
       private final static CaloriesService caloriesService = new CaloriesService();
     //  private final static Form<TestForm> testForm = Form.form(TestForm.class); // HIER GAAT HET MIS
    
 
	
	public static Result index() {
		ArrayList<Product> products = caloriesService.getProducts();

        Index index = new Index();
        index.setProducts(products);
//
//        return ok(views.html.QuestionController.index.render(index));
    	return ok(views.html.CaloriesController.index.render(index));
		//return ok(views.html.CaloriesController.index.render()); 
	} 
	
	
	 public static Result addCalories(){
   	  return ok(views.html.CaloriesController.addCalories.render());
     }
	 
	 public static Result edit(long id) {

		 ArrayList<Product> products = caloriesService.getProduct(id);
		 
		 Index index = new Index();
		 index.setProducts(products);
   	  return ok(views.html.CaloriesController.edit.render(index));
     } 

	public static Result submit(){
		DynamicForm requestData = Form.form().bindFromRequest();
		DependencyParser parser = new Alpino();
		
		
		try{
			ArrayList<NormalisedText> products = new ArrayList<>();
			for(String key: request().body().asFormUrlEncoded().keySet()){
				parser.parseText(requestData.data().get(key));
				products.add(parser.getOutPut());
			}
			caloriesService.saveProductsToDB(products);	
		}
		
		catch(Exception ex) {
			//LogService.getInstance().error.(ex.getMessage());
			ex.printStackTrace();
			return ok(views.html.CaloriesController.test.render());
		}
		return redirect(routes.CaloriesController.index());
	} 
	 
//	public static Result change(long id) {
//		// Hier moet het id worden meegegeven (zelfde manier als bij edit()),
//		// zodat de informatie van dat antwoord wordt aangepast
//		// Ook moet hierin de informatie uit de pagina worden uitgelezen
//		// Deze informatie moet worden opgeslagen in een ArrayList net als bij
//		// submit()
//		// NIET VERGETEN: (id:Long) zetten bij routes
//		 DynamicForm requestData = Form.form().bindFromRequest();
//		// Form<QuestionForm> filledForm = questionForm.bindFromRequest();
//		 DependencyParser parser = new Alpino();
//		 try {
//		 ArrayList<NormalisedText> products = new ArrayList<>();
//		//
//		// // QuestionForm form = new QuestionForm();
//		 for (String key : request().body().asFormUrlEncoded().keySet()) {
//		// if (!key.startsWith("answer")
//		// || requestData.data().get(key).equals(""))
//		// continue;
//		 parser.parseText(requestData.data().get(key));
//		 products.add(parser.getOutPut());
//		 }
//		// // return ok(views.html.QuestionController.test.render());
//		 caloriesService.changeProductInDB(id,products);
//		}
//		//
//		 catch (Exception ex) {
//		 LogService.getInstance().error(ex.getMessage());
//		 ex.printStackTrace();
//		 }
//		return redirect(routes.CaloriesController.index());
//	}
          
          
}
