package controllers;


import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import java.util.ArrayList;

public class LoginController extends Controller {

	public static Result logout(){
		return ok(views.html.LoginController.logout.render());
	}
	
	public static Result newUser(){
		return ok(views.html.LoginController.newuser.render());
	}
}
