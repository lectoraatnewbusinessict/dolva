package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.mvc.Controller;
import play.mvc.Result;

public class ChainController extends Controller {

    public static Result index() {
        // Call the GET endpoint for fetching the chain analyses
        JsonNode chains = controllers.api.ChainController.getChainsJSON();
        return ok(views.html.ChainController.index.render(chains));
    }

    public static Result form() {
        return ok(views.html.ChainController.form.render());
    }

    public static Result view(Long id) {
        JsonNode chain = controllers.api.ChainController.getChainJSON(id);
        if (chain == null) {
            return notFound();
        }
        return ok(views.html.ChainController.view.render(chain));
    }

}
