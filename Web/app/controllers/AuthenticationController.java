package controllers;

import play.*;
import play.mvc.*;
import play.data.DynamicForm;
import play.data.Form;

import views.html.*;
import views.AuthenticationController.View;
import models.Account;

import services.ControlPanelService;
import services.AuthenticationService;

import java.util.ArrayList;


public class AuthenticationController extends Controller {

	public static Result index() {
		View view = new View();
		System.out.println(Application.isConnected());
		if(Application.isConnected()){
			ControlPanelService service = new ControlPanelService();
			System.out.println(session("connected") + "<<<< DIT IS DE USERNAME IN SESSIE");
			ArrayList<Account> array = service.getAccount(session("connected"));
			if(array.size() > 0)
			{
				view.setAccount(array.get(0));
			}
		}
		return ok(views.html.AuthenticationController.index.render(view));
	}

	public static Result login() {

		DynamicForm requestData = Form.form().bindFromRequest();
		AuthenticationService authenticator = new AuthenticationService();
		ArrayList<String> userDetails = new ArrayList<>();
		try {
			ArrayList<String> loginDetails = new ArrayList<>();
			for (String key : request().body().asFormUrlEncoded().keySet()) {
				String temp = requestData.data().get(key);
				loginDetails.add(temp);
			}
			System.out.println("LOGGER:: ACtest ::");
			System.out.println("LOGGER:: "+loginDetails.get(0)+" :: " + loginDetails.get(1));
			userDetails = authenticator.AuthenticateUser(loginDetails.get(0), loginDetails.get(1));	
			if(userDetails.size() > 0){
				System.out.println("LOGGER:: ACtest2 ::");
				session("connected", userDetails.get(0));
				System.out.println("LOGGER:: ACtest3 ::");
				session("role", userDetails.get(1));
				System.out.println("LOGGER:: ACtest4 ::");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return redirect(routes.AuthenticationController.index());
		}
		return redirect(routes.AuthenticationController.index());
	}

	
	
	public static Result register() {
		View view = new View();
		return ok(views.html.AuthenticationController.index.render(view));
	}
	
	public static Result noAccess() {
		return ok(views.html.AuthenticationController.noAccess.render());
	}
	
	public static Result logout() {
		session().remove("connected");
		session().remove("role");
		return redirect(routes.AuthenticationController.index());
	}

}