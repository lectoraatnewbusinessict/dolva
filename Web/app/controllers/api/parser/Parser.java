package controllers.api.parser;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.api.parser.chain.StepParserFactory;
import controllers.api.parser.diary.DiaryParserFactory;
import models.BaseModel;

/**
 * Parses a specific part of a chain analysis or diary entry for saving to the database. Use {@link StepParserFactory}
 * or {@link DiaryParserFactory} for constructing instances.
 */
public interface Parser {

    public BaseModel parse(JsonNode json, BaseModel lastModel) throws IncompleteDataException;

}
