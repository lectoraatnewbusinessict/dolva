package controllers.api.parser.diary;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.api.parser.AbstractParser;
import controllers.api.parser.IncompleteDataException;
import models.BaseModel;
import models.diary.DiaryModel;

public class ObstaclesParser extends AbstractParser {

    @Override
    public BaseModel parse(JsonNode json, BaseModel lastModel) throws IncompleteDataException {
        // Check if the JSON has a valid structure.
        String question = this.getString(json, "question");

        // Build the base model and relate it to the previous one.
        DiaryModel entryModel = new DiaryModel();
        entryModel.addProperty("text", this.getNodeName());
        entryModel.create();
        lastModel.relateTo(entryModel.getId(), "ENTRY");

        // Build the question model and relate it to the entry model.
        DiaryModel questionModel = new DiaryModel();
        questionModel.addProperty("text", question);
        questionModel.create();
        entryModel.relateTo(questionModel.getId(), "QUESTION");

        // Build the answer models.
        for(JsonNode node : this.getArray(json, "answer")) {
            if (!node.isObject()) {
                throw new IncompleteDataException("Obstacles answer array should contain only objects");
            }

            DiaryModel answerModel = new DiaryModel();
            answerModel.addProperty("text", this.getString(node, "text"));
            answerModel.addProperty("intensity", String.valueOf(this.getInt(node, "intensity", 0, 6)));
            answerModel.create();
            questionModel.relateTo(answerModel.getId(), "ANSWER");
        }

        return entryModel;
    }

    protected String getNodeName() {
        return "obstacles";
    }
}
