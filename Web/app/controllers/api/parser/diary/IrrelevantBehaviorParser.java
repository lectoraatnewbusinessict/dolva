package controllers.api.parser.diary;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.api.parser.AbstractParser;
import controllers.api.parser.IncompleteDataException;
import models.BaseModel;
import models.diary.DiaryModel;

public class IrrelevantBehaviorParser extends AbstractParser {

    @Override
    public BaseModel parse(JsonNode json, BaseModel lastModel) throws IncompleteDataException {
        // Check if the JSON has a valid structure.
        String question = this.getString(json, "question");
        String answer = this.getString(json, "answer");
        String explanation = this.getString(json, "explanation", true);

        // Build the base model and relate it to the previous one.
        DiaryModel entryModel = new DiaryModel();
        entryModel.addProperty("text", this.getNodeName());
        entryModel.create();
        lastModel.relateTo(entryModel.getId(), "ENTRY");

        // Build the question model and relate it to the entry model.
        DiaryModel questionModel = new DiaryModel();
        questionModel.addProperty("text", question);
        questionModel.create();
        entryModel.relateTo(questionModel.getId(), "QUESTION");

        // Build the answer model. The first model is the main one, the rest is saved as 'explanation'.
        DiaryModel answerModel = new DiaryModel();
        answerModel.addProperty("text", answer);
        answerModel.create();
        questionModel.relateTo(answerModel.getId(), "ANSWER");

        if(!explanation.equals("")){
            DiaryModel explanationModel = new DiaryModel();
            explanationModel.addProperty("text", explanation);
            explanationModel.create();
            answerModel.relateTo(explanationModel.getId(), "EXPLANATION");
        }

        return entryModel;
    }

    protected String getNodeName() {
        return "irrelevantBehavior";
    }

}
