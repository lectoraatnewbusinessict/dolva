package controllers.api.parser.diary;

import controllers.api.parser.Parser;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class DiaryParserFactory {

    /**
     * Maps the names of keys in the sent JSON object to their respective Parser subclasses.
     */
    private static final Map<String, Class<? extends Parser>> PARSERS = new HashMap<>();
    static {
        PARSERS.put("feelings", FeelingsParser.class);
        PARSERS.put("obstacles", ObstaclesParser.class);
        PARSERS.put("irrelevantBehavior", IrrelevantBehaviorParser.class);
    }

    /**
     * Construct a Diary parser instance for the given key name. The key is part of the JSON object sent to the chain
     * analysis POST endpoint.
     * @param key The name of a key in the sent JSON object to get a Parser for.
     * @return A Parser for the given key, or null if none was found for that key.
     */
    public static Parser makeDiaryParser(String key) {
        Class<? extends Parser> clazz = PARSERS.get(key);
        if (clazz == null) {
            return null;
        }

        try {
            return clazz.getConstructor().newInstance();
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            return null;
        }
    }

}
