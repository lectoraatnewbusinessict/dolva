package controllers.api.parser;

public class MissingKeysException extends IncompleteDataException {

    private final String[] missingKeys;

    public MissingKeysException(String... missingKeys) {
        this.missingKeys = missingKeys;
    }

    @Override
    public String getMessage() {
        return "Sent JSON object is missing the following keys: " + String.join(", ", this.missingKeys);
    }

}
