package controllers.api.parser;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Abstract Parser class with some convenient methods for reading JSON nodes.
 */
public abstract class AbstractParser implements Parser {

    /**
     * Verifies that a JSON object exists in another JSON object, returning it if it does.
     * @param json The object JsonNode in which to find the JSON object.
     * @param key The key under which the JSON object should be located.
     * @param allowEmpty Whether the returned JSON object is allowed to be empty.
     * @return The found JSON object in the JsonNode.
     * @throws IncompleteDataException If the JSON object was not found, or the found JsonNode is not an object.
     */
    protected final JsonNode getObject(JsonNode json, String key, boolean allowEmpty) throws IncompleteDataException {
        JsonNode node = json.findPath(key);
        if (!node.isObject() || (!allowEmpty && node.size() == 0)) {
            throw new MissingKeysException(key);
        }
        return node;
    }

    protected final JsonNode getObject(JsonNode json, String key) throws IncompleteDataException {
        return this.getObject(json, key, false);
    }

    /**
     * Verifies that a JSON array exists in a JSON object, returning it if it does.
     * @param json The object JsonNode in which to find the JSON array.
     * @param key The key under which the JSON array should be located.
     * @param allowEmpty Whether the returned JSON array is allowed to be empty.
     * @return The found JSON array in the JsonNode.
     * @throws IncompleteDataException If the JSON array was not found, or the found JsonNode is not an array.
     */
    protected final JsonNode getArray(JsonNode json, String key, boolean allowEmpty) throws IncompleteDataException {
        JsonNode node = json.findPath(key);
        if (!node.isArray() || (!allowEmpty && node.size() == 0)) {
            throw new MissingKeysException(key);
        }
        return node;
    }

    protected final JsonNode getArray(JsonNode json, String key) throws IncompleteDataException {
        return this.getArray(json, key, false);
    }

    /**
     * Verifies that a string exists in a JSON object, returning it if it does.
     * @param json The object JsonNode in which to find the string.
     * @param key The key under which the string should be located.
     * @param allowEmpty Whether the returned string is allowed to be empty.
     * @return The found string in the JsonNode.
     * @throws IncompleteDataException If the string was not found, or the found JsonNode is not a string.
     */
    protected final String getString(JsonNode json, String key, boolean allowEmpty) throws IncompleteDataException {
        String text = json.findPath(key).asText();
        if (!allowEmpty && text.equals("")) {
            throw new MissingKeysException(key);
        }
        return text;
    }

    protected final String getString(JsonNode json, String key) throws IncompleteDataException {
        return this.getString(json, key, false);
    }

    /**
     * Verifies that an integer exists in a JSON object, returning it if it does.
     * @param json The object JsonNode in which to find the integer.
     * @param key The key under which the string should be located.
     * @param min The minimum allowed value of the integer.
     * @param max The maximum allowed value of the integer.
     * @return The found integer in the JsonNode.
     * @throws IncompleteDataException If the integer value is not between the given minimum and maximum.
     */
    protected final int getInt(JsonNode json, String key, int min, int max) throws IncompleteDataException {
        int value = json.findPath(key).asInt();
        if (value < min || value > max) {
            throw new IncompleteDataException(String.format("Integer '%s' should be between %d and %d (inclusive)", key, min, max));
        }
        return value;
    }

}
