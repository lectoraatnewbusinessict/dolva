package controllers.api.parser.chain;

public class PreventionStrategyParser extends QuestionAnswerParser {

    @Override
    protected String getStepNodeName() {
        return "preventionStrategy";
    }

}
