package controllers.api.parser.chain;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.api.parser.AbstractParser;
import controllers.api.parser.IncompleteDataException;
import models.BaseModel;
import models.chain.ChainAnalysisModel;

public abstract class QuestionAnswerParser extends AbstractParser {

    @Override
    public BaseModel parse(JsonNode json, BaseModel lastModel) throws IncompleteDataException {
        String question = this.getString(json, "question");
        String answer = this.getString(json, "answer");

        ChainAnalysisModel stepModel = new ChainAnalysisModel();
        stepModel.addProperty("text", this.getStepNodeName());
        stepModel.create();
        lastModel.relateTo(stepModel.getId(), "STEP");

        ChainAnalysisModel questionModel = new ChainAnalysisModel();
        questionModel.addProperty("text", question);
        questionModel.create();
        stepModel.relateTo(questionModel.getId(), "QUESTION");

        ChainAnalysisModel answerModel = new ChainAnalysisModel();
        answerModel.addProperty("text", answer);
        answerModel.create();
        questionModel.relateTo(answerModel.getId(), "ANSWER");

        return stepModel;
    }

    protected abstract String getStepNodeName();

}
