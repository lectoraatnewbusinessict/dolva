package controllers.api.parser.chain;

public class ProvocationParser extends QuestionAnswerParser {

    @Override
    protected String getStepNodeName() {
        return "provocation";
    }

}
