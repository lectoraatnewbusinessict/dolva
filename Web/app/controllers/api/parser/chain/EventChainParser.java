package controllers.api.parser.chain;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.api.parser.AbstractParser;
import controllers.api.parser.IncompleteDataException;
import controllers.api.parser.MissingKeysException;
import models.BaseModel;
import models.chain.ChainAnalysisModel;

import java.util.*;

public class EventChainParser extends AbstractParser {

    private static final List<String> KEYS = Arrays.asList("activity", "event", "sensation", "feeling", "cognition");

    @Override
    public BaseModel parse(JsonNode json, BaseModel lastModel) throws IncompleteDataException {
        // Check if the JSON has a valid structure.
        String question = this.getString(json, "question");
        JsonNode answer = this.getArray(json, "answer");

        // Build the base step model and relate it to the previous one.
        ChainAnalysisModel stepModel = new ChainAnalysisModel();
        stepModel.addProperty("text", "eventChain");
        stepModel.create();
        lastModel.relateTo(stepModel.getId(), "STEP");

        // Build the question model and relate it to the step model.
        ChainAnalysisModel questionModel = new ChainAnalysisModel();
        questionModel.addProperty("text", question);
        questionModel.create();
        stepModel.relateTo(questionModel.getId(), "QUESTION");

        // Keep track of the count of each event chain type, as every type must occur at least once.
        Map<String, Integer> counts = new HashMap<>();
        for (String s : KEYS) {
            counts.put(s, 0);
        }

        // Build the event chain models.
        ChainAnalysisModel prev = questionModel;
        for (JsonNode node : answer) {
            if (!node.isObject() || node.size() == 0) {
                throw new IncompleteDataException("Event chain array should only contain objects");
            }

            String type = this.getString(node, "type");
            if (!KEYS.contains(type)) {
                throw new IncompleteDataException(String.format("Event chain type %s not in %s", type, KEYS.toString()));
            }
            counts.put(type, counts.get(type) + 1);

            ChainAnalysisModel model = new ChainAnalysisModel();
            model.addProperty("type", type);
            model.addProperty("text", this.getString(node, "text"));
            model.create();

            String prevention = this.getString(node, "prevention", true);
            if (!prevention.equals("")) {
                ChainAnalysisModel preventionModel = new ChainAnalysisModel();
                preventionModel.addProperty("text", prevention);
                preventionModel.create();
                model.relateTo(preventionModel.getId(), "PREVENTION");
            }

            prev.relateTo(model.getId(), "EVENT_CHAIN");
            prev = model;
        }

        // Check if each event chain type occurred at least once.
        List<String> missing = new ArrayList<>();
        for (Map.Entry<String, Integer> e : counts.entrySet()) {
            if (e.getValue() < 1) {
                missing.add(e.getKey());
            }
        }
        if (missing.size() > 0) {
            throw new MissingKeysException(missing.toArray(new String[missing.size()]));
        }

        return stepModel;
    }

}
