package controllers.api.parser.chain;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.api.parser.AbstractParser;
import controllers.api.parser.IncompleteDataException;
import controllers.api.parser.MissingKeysException;
import models.BaseModel;
import models.chain.ChainAnalysisModel;

public class ProblemBehaviorParser extends AbstractParser {

    @Override
    public BaseModel parse(JsonNode json, BaseModel lastModel) throws IncompleteDataException {
        // Check if the JSON has a valid structure.
        String question = this.getString(json, "question");
        JsonNode answer = this.getArray(json, "answer");
        JsonNode food = this.getArray(json, "food");

        // Build the base step model and relate it to the previous one.
        ChainAnalysisModel stepModel = new ChainAnalysisModel();
        stepModel.addProperty("text", "problemBehavior");
        stepModel.create();
        lastModel.relateTo(stepModel.getId(), "STEP");

        // Build the question model and relate it to the step model.
        ChainAnalysisModel questionModel = new ChainAnalysisModel();
        questionModel.addProperty("text", question);
        questionModel.create();
        stepModel.relateTo(questionModel.getId(), "QUESTION");

        // The main answer model.
        ChainAnalysisModel answerModel = null;

        // Build the answer model(s). The first model is the main one, the rest is saved as 'more information'.
        boolean first = true;
        ChainAnalysisModel prev = questionModel;
        for (JsonNode node : answer) {
            String text = node.asText();
            if (text.equals("")) {
                continue;
            }

            ChainAnalysisModel model = new ChainAnalysisModel();
            model.addProperty("text", text);
            model.create();

            if (first) {
                prev.relateTo(model.getId(), "ANSWER");
                answerModel = model;
                first = false;
            } else {
                prev.relateTo(model.getId(), "MORE");
            }

            prev = model;
        }

        // The answer model should be set by now.
        if (answerModel == null) {
            throw new MissingKeysException("answer");
        }

        // Build the model(s) that describe the consumed food and relate them to the main answer model.
        for (JsonNode node : food) {
            String text = node.asText();
            if (text.equals("")) {
                continue;
            }

            ChainAnalysisModel foodModel = new ChainAnalysisModel();
            foodModel.addProperty("text", text);
            foodModel.create();
            answerModel.relateTo(foodModel.getId(), "FOOD");
        }

        return stepModel;
    }

}
