package controllers.api.parser.chain;

import com.fasterxml.jackson.databind.JsonNode;
import controllers.api.parser.AbstractParser;
import controllers.api.parser.IncompleteDataException;
import models.BaseModel;
import models.chain.ChainAnalysisModel;

public class ConsequencesParser extends AbstractParser {

    @Override
    public BaseModel parse(JsonNode json, BaseModel lastModel) throws IncompleteDataException {
        // Get the question string and answer array from the object.
        String question = this.getString(json, "question");
        JsonNode answer = this.getArray(json, "answer");

        // Build the model that describes the step in the chain analysis.
        ChainAnalysisModel stepModel = new ChainAnalysisModel();
        stepModel.addProperty("text", "consequences");
        stepModel.create();
        lastModel.relateTo(stepModel.getId(), "STEP");

        // Build the model that contains the question.
        ChainAnalysisModel questionModel = new ChainAnalysisModel();
        questionModel.addProperty("text", question);
        questionModel.create();
        stepModel.relateTo(questionModel.getId(), "QUESTION");

        // Add the answer models, one by one.
        for (JsonNode node : answer) {
            String text = this.getString(node, "text");
            String repair = this.getString(node, "repair", true);

            ChainAnalysisModel answerModel = new ChainAnalysisModel();
            answerModel.addProperty("text", text);
            answerModel.create();
            questionModel.relateTo(answerModel.getId(), "ANSWER");

            if (!repair.equals("")) {
                ChainAnalysisModel repairModel = new ChainAnalysisModel();
                repairModel.addProperty("text", repair);
                repairModel.create();
                answerModel.relateTo(repairModel.getId(), "REPAIR");
            }
        }

        return stepModel;
    }

}
