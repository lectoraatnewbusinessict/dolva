package controllers.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.api.parser.IncompleteDataException;
import controllers.api.parser.MissingKeysException;
import controllers.api.parser.chain.StepParserFactory;
import controllers.api.parser.Parser;
import models.BaseModel;
import models.chain.ChainAnalysisMetaModel;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.ChainAnalysisService;

import java.util.List;

public class ChainController extends Controller {

    private static final ChainAnalysisService SERVICE = new ChainAnalysisService();

    /**
     * <p>Send a POST request to save a chain analysis to the database.</p>
     *
     * <p>The request body should be a JSON object that looks like this:</p>
     *
     * <pre>
     * {
     *     "name": "Ton Broekhuizen",
     *     "analysisDate": 1444582383,
     *     "behaviorDate": 1444395600,
     *     "problemBehavior": {
     *         "question": "Ik had een...",
     *         "answer": [
     *             "eetaanval, eetbui",
     *             "klein, maar wel objectief: snaaiaanval"
     *         ],
     *         "food": [
     *             "ijs",
     *             "snoep",
     *             "brood"
     *         ]
     *     },
     *     "provocation": {
     *         "question": "Welke gebeurtenis vond er plaats? Wanneer begon het? Wat was er aan de hand?",
     *         "answer": "Ik kwam net thuis van een slechte dag op school en begon meteen de koelkast leeg te eten"
     *     },
     *     "vulnerabilityFactors": {
     *         "question": "Welke van deze factoren maakten je kwetsbaar voor de uitlokkende gebeurtenis?",
     *         "answer": [
     *             {
     *                 "category": "gerelateerd met voeding",
     *                 "factors": [
     *                     {"text": "ik was op dieet", "importance": 4},
     *                     {"text": "ik had nog niet gegeten", "importance": 2}
     *                 ]
     *             }
     *         ]
     *     },
     *     "eventChain": {
     *         "question": "Hoe verliep het proces tussen aanleiding en probleemgedrag precies?",
     *         "answer": [
     *             {
     *                 "type": "activity",
     *                 "text": "ik was tv aan het kijken",
     *                 "prevention": "ik had eigenlijk huiswerk moeten maken"
     *             },
     *             {
     *                 "type": "event",
     *                 "text": "mijn ouders kregen ruzie met elkaar"
     *             },
     *             {
     *                 "type": "sensation",
     *                 "text": "gevoel van uitputting"
     *             },
     *             {
     *                 "type": "feeling",
     *                 "text": "machteloos",
     *                 "prevention": "Ik had kunnen proberen de ruzie te stoppen"
     *             },
     *             {
     *                 "type": "cognition",
     *                 "text": "ik ben schuldig"
     *             }
     *         ]
     *     },
     *     "consequences": {
     *         "question": "Wat waren de directe gevolgen van je gedrag?",
     *         "answer": [
     *             {
     *                 "text": "ik voelde me vies, ik ben vast en zeker weer aangekomen"
     *             },
     *             {
     *                 "text": "de koelkast is door mij leeggegeten",
     *                 "repair": "ik ga voor mijn ouders naar de supermarkt"
     *             }
     *         ]
     *     },
     *     "preventionStrategy": {
     *         "question": "De volgende keer dat me dit overkomt, ga ik:",
     *         "answer": "mijn hulplijn bellen"
     *     }
     * }
     * </pre>
     *
     * @return A Result with status 200 and a JSON object that contains the saved chain analysis's ID if the data was
     *         successfully saved to the database, or with status 400 if the request body is not JSON or is missing
     *         required values.
     */
    public static Result postChain() {
        ObjectNode result = Json.newObject();

        // Get the JSON body sent by the client in the POST request.
        JsonNode json = request().body().asJson();
        if (json == null) {
            result.put("error", "No JSON data found");
            return badRequest(result);
        }

        // Get the chain analysis's metadata.
        String name = json.findPath("name").asText();
        long analysisDate = json.findPath("analysisDate").asLong();
        long behaviorDate = json.findPath("behaviorDate").asLong();

        ChainAnalysisMetaModel metadataModel = null;
        try {
            // Check if the metadata is valid.
            if (name.equals("")) {
                throw new MissingKeysException("name");
            } else if (analysisDate <= 0) {
                throw new MissingKeysException("analysisDate");
            } else if (behaviorDate <= 0) {
                throw new MissingKeysException("behaviorDate");
            }

            // Save the chain analysis metadata to the database.
            metadataModel = new ChainAnalysisMetaModel(name, analysisDate, behaviorDate);
            metadataModel.create();

            // Save the chain analysis data to the database, part by part.
            parse("problemBehavior", json, metadataModel);
            parse("provocation", json, metadataModel);
            parse("vulnerabilityFactors", json, metadataModel);
            parse("eventChain", json, metadataModel);
            parse("consequences", json, metadataModel);
            parse("preventionStrategy", json, metadataModel);

            result.put("id", metadataModel.getId());

        } catch (IncompleteDataException e) {
            e.printStackTrace();

            // Delete the incomplete chain from the database.
            if (metadataModel != null) {
                metadataModel.deleteAllFromHere();
            }

            result.put("error", e.getMessage());
            return badRequest(result);
        }

        return ok(result);
    }

    /**
     * Small utility method that calls the proper Parser for a key in the sent JSON object.
     */
    private static BaseModel parse(String key, JsonNode json, BaseModel lastModel) throws IncompleteDataException {
        JsonNode node = json.findPath(key);
        if ((!node.isObject() && !node.isArray()) || node.size() == 0) {
            throw new MissingKeysException(key);
        }

        Parser parser = StepParserFactory.makeStepParser(key);
        if (parser == null) {
            return lastModel;
        }
        return parser.parse(json.findPath(key), lastModel);
    }

    /**
     * @return A Play Result containing a JSON object with all stored chain analysis data.
     */
    public static Result getChains() {
        return ok(getChainsJSON());
    }

    /**
     * @return A JsonNode with all stored chain analysis data, in the following format: <code>{items: [], count: 1}</code>
     */
    public static JsonNode getChainsJSON() {
        List<ChainAnalysisMetaModel> metaModels = SERVICE.getMetadatas();

        ObjectNode result = Json.newObject();
        ArrayNode array = result.putArray("items");
        for (ChainAnalysisMetaModel m : metaModels) {
            array.add(m.toJSON(SERVICE));
        }

        result.put("count", metaModels.size());

        return result;
    }

    /**
     * @return A Play Result containing a JSON object with the data of the chain analysis with the given ID,
     *         or a 404 error if no chain analysis with the given ID exists.
     */
    public static Result getChain(long id) {
        JsonNode chain = getChainJSON(id);
        if (chain == null) {
            return notFound();
        }
        return ok(chain);
    }

    /**
     * @return A JsonNode with the data of the chain analysis with the given ID,
     *         or null if no chain analysis with the given ID exists.
     */
    public static JsonNode getChainJSON(long id) {
        ChainAnalysisMetaModel metaModel = SERVICE.getMetadataById(id);
        if (metaModel == null) {
            return null;
        }
        return metaModel.toJSON(SERVICE);
    }

}
