package controllers.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.QuestionService;

public class ChatController extends Controller {
    private static final QuestionService questionService = new QuestionService();

    public static Result askQuestion() {
        ObjectNode result = Json.newObject();
        JsonNode json = request().body().asJson();

        if (json == null) {
            result.put("error", "No JSON data found");
            return badRequest(result);
        }

        String question = json.findPath("category").asText();

        if (question == null || question.equals("")) {
            result.put("error", "Postdata doesn't contain 'category'");
            return badRequest(result);
        }

        result.put("answer", questionService.askQuestion(question));

        return ok(result);
    }
}
