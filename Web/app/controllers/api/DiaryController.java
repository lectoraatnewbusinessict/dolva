package controllers.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.api.parser.diary.DiaryParserFactory;
import controllers.api.parser.IncompleteDataException;
import controllers.api.parser.MissingKeysException;
import controllers.api.parser.Parser;
import models.BaseModel;
import models.diary.DiaryMetaModel;
import models.diary.DiaryModel;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.DiaryService;

import java.util.List;


public class DiaryController extends Controller {

    private static final DiaryService SERVICE = new DiaryService();

    /**
     * <p>Send a POST request to save a diary entry to the database.</p>
     *
     * <p>The request body should be a JSON object that looks like this:</p>
     *
     * <pre>
     * {
     *     "name": "Ton Broekhuizen",
     *     "date": 1444395600,
     *     "feelings": {
     *         question: "Hoe gaat het vandaag met je?",
     *         answer: "Kon minder"
     *     },
     *     "obstacles": {
     *         question: "Was er vandaag sprake van:",
     *         "answer": [
     *             {"text": "een eetbui of snaaiaanval", "intensity": 3},
     *             {"text": "eten zonder bewuste aandacht", "intensity": 1}
     *         ]
     *     },
     *     "irrelevantBehavior" : {
     *         question: "Was er sprake van zgn. irrelevant gedrag?",
     *         answer: "ja",
     *         explanation: "Het was geen goede dag vandaag"
     *     }
     *}
     * </pre>
     *
     * @return A Result with status 200 and a JSON object that contains the saved diary's ID if the data was
     *         successfully saved to the database, or with status 400 if the request body is not JSON or is missing
     *         required values.
     */
    public static Result postDiary() {
        ObjectNode result = Json.newObject();

        // Get the JSON body sent by the client in the POST request.
        JsonNode json = request().body().asJson();
        if (json == null) {
            result.put("error", "No JSON data found");
            return badRequest(result);
        }

        // Get the diary entry's metadata.
        String name = json.findPath("name").asText();
        long date = json.findPath("date").asLong();

        DiaryMetaModel metadata = null;
        try {
            // Check if the metadata is valid.
            if (name.equals("")) {
                throw new MissingKeysException("name");
            } else if (date <= 0) {
                throw new MissingKeysException("date");
            }

            // Save the diary entry metadata to the database.
            metadata = new DiaryMetaModel(name, date);
            metadata.create();

            // Save the diary entry model to the database, part by part.
            parse("feelings", json, metadata);
            parse("obstacles", json, metadata);
            parse("irrelevantBehavior", json, metadata);

            result.put("id", metadata.getId());

        } catch (IncompleteDataException e) {
            e.printStackTrace();

            // Delete the incomplete chain from the database.
            if (metadata != null) {
                metadata.deleteAllFromHere();
            }

            result.put("error", e.getMessage());
            return badRequest(result);
        }
        return ok(result);
    }

    /**
     * <p>Send a POST request to save a comment to a diary entry.</p>
     *
     * <p>The request body should be a JSON object that looks like this:</p>
     *
     * <pre>
     * {
     *     "comment": "Ik was erg emotioneel tijdens het invullen."
     * }
     * </pre>
     *
     * @param id The id of the meta node, where the comment should relate to.
     *
     * @return A Result with status 200 and an empty JSON object if the data was successfully saved to the database,
     *         or with status 400 if the request body is not JSON or is missing required values.
     */
    public static Result postDiaryComment(Long id) {
        // Get the diary entry from the database. If it fails, return a 404.
        DiaryMetaModel metaModel = SERVICE.getMetadataById(id);

        if (metaModel == null) {
            return notFound();
        }

        // Get the JSON body sent by the client in the PUT request.
        ObjectNode result = Json.newObject();

        JsonNode json = request().body().asJson();
        if (json == null) {
            result.put("error", "No JSON data found");
            return badRequest(result);
        }

        //Get the comment form the request
        String comment = json.findPath("comment").asText();

        // Parse the comment, and add it to the database.
        // Build the base model and relate it to the previous one.
        DiaryModel commentModel = new DiaryModel();
        commentModel.addProperty("text", comment);
        commentModel.create();
        metaModel.relateTo(commentModel.getId(), "COMMENT");

        return ok(result);
    }

    /**
     * Small utility method that calls the proper Parser for a key in the sent JSON object.
     */
    private static BaseModel parse(String key, JsonNode json, BaseModel lastModel) throws IncompleteDataException {
        JsonNode node = json.findPath(key);
        if ((!node.isObject() && !node.isArray()) || node.size() == 0) {
            throw new MissingKeysException(key);
        }

        Parser parser = DiaryParserFactory.makeDiaryParser(key);
        if (parser == null) {
            return lastModel;
        }
        return parser.parse(json.findPath(key), lastModel);
    }

    /**
     * @return A Play Result containing a JSON object with all stored diary data.
     */
    public static Result getDiaries() {
        return ok(getDiariesJSON());
    }

    /**
     * @return A JsonNode with all stored diary data, in the following format: <code>{items: [], count: 1}</code>
     */
    public static JsonNode getDiariesJSON() {
        List<DiaryMetaModel> metaModels = SERVICE.getMetadatas();

        ObjectNode result = Json.newObject();
        ArrayNode array = result.putArray("items");
        for (DiaryMetaModel m : metaModels) {
            array.add(m.toJSON(SERVICE));
        }

        result.put("count", metaModels.size());

        return result;
    }

    /**
     * @return A Play Result (200) containing a JSON object with the data of the diary with the given ID,
     *         or a 404 error if no diary with the given ID exists.
     */
    public static Result getDiary(long id) {
        JsonNode diary = getDiaryJSON(id);
        if (diary == null) {
            return notFound();
        }
        return ok(diary);
    }

    /**
     * @return A JsonNode with the data of the diary with the given ID,
     *         or null if no diary with the given ID exists.
     */
    public static JsonNode getDiaryJSON(long id) {
        DiaryMetaModel metaModel = SERVICE.getMetadataById(id);
        if (metaModel == null) {
            return null;
        }
        return metaModel.toJSON(SERVICE);
    }

}
