package controllers;

import play.data.DynamicForm;
import play.data.Form;

import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Call;
import play.mvc.Http;
import play.mvc.Http.Context;
import play.mvc.Http.Session;

import services.ControlPanelService;

import models.Account;

import java.util.ArrayList;

import views.ControlPanelController.ViewAll;

/*
 * Execute query below for administrator account with login: Administrator || password: Administrator
 * CREATE (u:Username { name : 'Administrator', surname : 'Administrator', username : 'administrator', password : 'ba0149941e9dc4f226b95dba551668ce4b0d0398c92536b5705fe2e1b1a8c227452717420549d37881d11bddb6b95a295537782690d90a919139aebad7775d34', salt : '[B@26db31e6',date : 'Mon Feb 08 2016',role : 'Admin'})
 * 
 * 
 * 
 */
public class ControlPanelController extends Controller {
	
	public static Result index() {
		if(Application.isConnected() && Application.isAdmin()){
			ControlPanelService service = new ControlPanelService();
			System.out.println(session("connected") + "<<<< DIT IS DE USERNAME IN SESSIE");
		ViewAll view = new ViewAll();
		view.setAccounts(service.getAccounts());
		return ok(views.html.ControlPanelController.index.render(view));
		}else{
			return redirect(routes.AuthenticationController.index());
		}
	}

	public static Result form() {
		if(Application.isConnected() && Application.isAdmin()){
			return ok(views.html.ControlPanelController.form.render());
		}else{
			return redirect(routes.AuthenticationController.index());
		}
	}
	
	public static Result delete(String login){
		if(Application.isConnected() && Application.isAdmin()){
			// roep delete methode aan in service
			ControlPanelService service = new ControlPanelService();
			ViewAll view = new ViewAll();
			view.setAccounts(service.getAccounts());
			session("connected", "userTest");
			return ok(views.html.ControlPanelController.index.render(view));
		}else{
			return redirect(routes.AuthenticationController.index());
		}
	}
	
	public static Result edit(String login){
		if(Application.isConnected() && Application.isAdmin()){
			// roep delete methode aan in service
			ControlPanelService service = new ControlPanelService();
			ViewAll view = new ViewAll();
			view.setAccounts(service.getAccount(login));
			return ok(views.html.ControlPanelController.edit.render(view));
		}else{
			return redirect(routes.AuthenticationController.index());
		}
	}

	public static Result submit() {
		if(Application.isConnected() && Application.isAdmin()){
			DynamicForm requestData = Form.form().bindFromRequest();
			ControlPanelService service = new ControlPanelService();
	
			try {
				ArrayList<String> accounts = new ArrayList<>();
				for (String key : request().body().asFormUrlEncoded().keySet()) {
					String temp = requestData.data().get(key);
					accounts.add(temp);
				}
				service.saveAnswersToDB(accounts);
			} catch (Exception e) {
				e.printStackTrace();
				return redirect(routes.ControlPanelController.index());
			}
	
			return redirect(routes.ControlPanelController.index());
		}else{
			return redirect(routes.AuthenticationController.index());
		}	
	}
}