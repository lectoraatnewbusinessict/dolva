package controllers;


import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import java.util.ArrayList;

public class PersonalController extends Controller {

	public static Result index(){
		return ok(views.html.PersonalController.index.render());
	}
	
	public static Result profile(){
		return ok(views.html.PersonalController.profile.render());
	} 
	
	public static Result settings(){
		return ok(views.html.PersonalController.personalsettings.render());
	}
	
	public static Result submit(){
		return ok(views.html.PersonalController.profile.render());
	}
	
}
