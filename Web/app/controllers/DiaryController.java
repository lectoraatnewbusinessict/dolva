package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.mvc.Controller;
import play.mvc.Result;

public class DiaryController extends Controller {

    public static Result index() {
        // Call the GET endpoint for fetching the diary entries
        JsonNode entries = controllers.api.DiaryController.getDiariesJSON();
        return ok(views.html.DiaryController.index.render(entries));
    }

    public static Result form() {
        return ok(views.html.DiaryController.form.render());
    }

    public static Result view(Long id) {
        JsonNode entry = controllers.api.DiaryController.getDiaryJSON(id);
        if (entry == null) {
            return notFound();
        }
        return ok(views.html.DiaryController.view.render(entry));
    }

    public static Result comment(Long id) {
        JsonNode entry = controllers.api.DiaryController.getDiaryJSON(id);
        if (entry == null) {
            return notFound();
        }
        return ok(views.html.DiaryController.comment.render(entry));
    }

}
