package controllers;

import models.Word;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import services.SynonymService;
import views.SynonymController.IndexModel;
import services.LogService;
import java.util.ArrayList;

public class SynonymController extends Controller {
    public static final SynonymService synonymService = new SynonymService();

    public static Result index() {
        return ok(views.html.SynonymController.index.render(
            new IndexModel(synonymService.getWords()))
        );
    }

    public static Result edit(long id) {
        Word word = synonymService.getById(id);

        return ok(views.html.SynonymController.edit.render(word));
    }

    public static Result delete(Long wordId, String item) {
        synonymService.delete(wordId, item);
        return redirect(routes.SynonymController.edit(wordId));
    }
    
    public static Result remove(long id){
    	ArrayList<Word> words = synonymService.getWord(id);
    	
    	IndexModel indexmodel = new IndexModel(words);
    
    	return ok(views.html.SynonymController.delete.render(indexmodel)
            );
    }
    
    public static Result deleteWord(long id){
    	try {
			synonymService.RemoveWord(id);
		}
		catch(Exception ex) {
			LogService.getInstance().error(ex.getMessage());
			ex.printStackTrace();
		}
		return redirect(routes.SynonymController.index());
    	
    }

    public static Result submit() {
        DynamicForm requestData = Form.form().bindFromRequest();
        String wordId = requestData.get("wordid");
        String synonym = requestData.get("synonym");

        if (!wordId.equals("") && !synonym.equals("")) {
            synonymService.addSynonym(Long.parseLong(wordId), synonym);
        }

        return redirect(routes.SynonymController.edit(Long.parseLong(wordId)));
    }
}
