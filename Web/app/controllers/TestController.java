package controllers;

import forms.QuestionForm;
import models.Answer;
import models.NormalisedText;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import services.LogService;
import services.QuestionService;
import services.parser.Alpino;
import services.parser.DependencyParser;
import views.TestController.Index;
import services.TestService;
import models.Meal;
//import views.TestController.Index; 

import java.util.ArrayList;

public class TestController extends Controller {
     // private final static Form<QuestionForm> questionForm = Form.form(QuestionForm.class);
       private final static TestService testService = new TestService();
     //  private final static Form<TestForm> testForm = Form.form(TestForm.class); // HIER GAAT HET MIS
    
 
    
    public static Result index() {
     ArrayList<Meal> meals = testService.getMeals();

        Index index = new Index();
        index.setMeals(meals);
//
//        return ok(views.html.QuestionController.index.render(index));
    	return ok(views.html.TestController.index.render(index));
    }

//    public static Result overview(){
//    	
//    	return ok(views.html.TestController.overview.render());
//    }
    
    
    
//    	public static Result overview(long id) {
//        // TODO: change to Neo4J REST API use graph object modelling?
//        QuestionForm form = new QuestionForm();
//
//        return ok(views.html.QuestionController.overview.render(questionForm.fill(form)));
//   }
//
          public static Result add() {
//        return ok(views.html.QuestionController.add.render(questionForm));
    		return ok(views.html.TestController.add.render());
   }
//          public static Result addCalories(){
//        	  return ok(views.html.TestController.addCalories.render());
//          }
//          
//          public static Result edit() {
//        	  
//        	  return ok(views.html.TestController.index.render());
//          } 

          
//          public static Result submit(){
//        	  
//        	  return ok(views.html.TestController.test.render());
//          }
//
          public static Result submit(){
        	  DynamicForm requestData = Form.form().bindFromRequest();
        	  DependencyParser parser = new Alpino();
        	 // String calories = "010203040506";
        	  
      	  try {
        		  ArrayList<NormalisedText> meals = new ArrayList<>(); 
        		  for(String key: request().body().asFormUrlEncoded().keySet()){
        			//if(!key.startsWith("meal")|| requestData.data().get(key).equals("")) continue; 
        			 parser.parseText(requestData.data().get(key)); 
        			 meals.add(parser.getOutPut());

     		  }
        		  testService.saveAnswersToDB(meals);
       	//	 return ok(views.html.TestController.test.render());
        	  }
        	  catch (Exception ex){
        		  LogService.getInstance().error(ex.getMessage());
        		  ex.printStackTrace();
        		  //return badRequest(ex.getMessage());
    		  return ok(views.html.TestController.test.render());
        	  }
        	  return redirect(routes.TestController.index());
          }
          
          
}
