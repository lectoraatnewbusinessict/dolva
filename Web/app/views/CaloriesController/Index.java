package views.CaloriesController;

import models.Product;

import java.util.ArrayList;

public class Index {
    public ArrayList<Product> products;

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(ArrayList<Product> products) {
        this.products = products;
    }
}
