package views.TestController;

import models.Meal;
import models.Question;

import java.util.ArrayList;

public class Index {
    public ArrayList<Meal> meals;

    public ArrayList<Meal> getMeals() {
        return meals;
    }

    public void setMeals(ArrayList<Meal> meals) {
        this.meals = meals;
    }
}
