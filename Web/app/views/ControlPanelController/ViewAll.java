package views.ControlPanelController;

import models.Account;
import java.util.ArrayList;

public class ViewAll {
    public ArrayList<Account> accounts;

    public ArrayList<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }
    
    public void addAccount(Account temp){
    	this.accounts.add(temp);
    }
}
