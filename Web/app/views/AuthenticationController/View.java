package views.AuthenticationController;

import models.Account;
import java.util.ArrayList;

public class View {
    public ArrayList<Account> accounts;
    public Account account;

    public ArrayList<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }
    
    public void addAccount(Account temp){
    	this.accounts.add(temp);
    }
    
    public void setAccount(Account temp){
    	this.account = temp;
    }
    
    public Account getAccount() {
    	return account;
    }
    
    public boolean isThereAnAccount(){
    	boolean temp = false;
    	if(this.account != null){ temp = true; }
    	return temp;
    }
}