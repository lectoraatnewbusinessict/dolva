package views.SynonymController;

import models.Word;

import java.util.ArrayList;

public class IndexModel {
    private ArrayList<Word> words;

    public IndexModel(ArrayList<Word> words) {
        this.setWords(words);
    }

    public ArrayList<Word> getWords() {
        return words;
    }

    public void setWords(ArrayList<Word> words) {
        this.words = words;
    }
}
