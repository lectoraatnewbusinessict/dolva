package views.QuestionController;

import models.Answer;
import models.Question;

import java.util.ArrayList;

public class Index {
    public ArrayList<Answer> answers;

    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
    }
}
