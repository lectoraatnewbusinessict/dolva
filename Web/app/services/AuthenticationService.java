package services;

import com.fasterxml.jackson.databind.JsonNode;

import models.Account;

import services.GraphDBService;
import services.ControlPanelService;
import services.SecurityService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AuthenticationService {
	
	SecurityService security = new SecurityService();
	ControlPanelService control = new ControlPanelService();
	
	/*
	 * Method to authenticate user. Returns arraylist with username + role, or empty when the user did not authenticate.
	 * @param username & password
	 * @return ArrayList<String> [0] = username, [1] = role
	 */
	
	public ArrayList<String> AuthenticateUser(String username, String password)
	{
		ArrayList<String> array = new ArrayList<>();
		System.out.println("LOGGER:: AStest ::");
		username = username.toLowerCase();
		if(!username.isEmpty() && !"".equals(username) && !password.isEmpty() && !"".equals(password))
		{
			System.out.println("LOGGER:: AStest2 ::");
			ArrayList<Account> user = new ArrayList<>();
			System.out.println("LOGGER:: AStest3 ::");
			//ArrayList<Account> accountInformation = ControlPanelService.getAccount(username);
			System.out.println("LOGGER:: AStest4 ::");
			boolean isAuthenticated = security.AuthenticateUser(username, password);
			System.out.println("LOGGER:: "+isAuthenticated+" ::");
			System.out.println("LOGGER:: AStest5 ::");
			if(isAuthenticated){

				user = control.getAccount(username);
				for(Account temp : user) 
				{
					array.add(username);
					array.add(temp.getRole());
				}
			}
		}
		return array;
	}
	
	
	
	
}	