package services.parser;


import models.NormalisedText;
import models.WordCategory;
import services.LogService;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public abstract class DependencyParser {
    private WordCategory category;

    public abstract String parseText(String text);

    public abstract NormalisedText getOutPut();

    protected String callExternalProgram(String path, List<String> cmdAndArgs) {
        String out = "";

        try {
            ProcessBuilder pb = new ProcessBuilder(cmdAndArgs);
            pb.directory(new File(path));
            Process p = pb.start();
            int exitStatus = p.waitFor();

            InputStream is = p.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;

            while ((line = br.readLine()) != null) {
                out += line;
            }

            out += exitStatus;
        } catch (Exception e) {
            LogService.getInstance().error(e.getMessage());
            e.printStackTrace();
        }

        return out;
    }

    public WordCategory getCategory() {
        return category;
    }

    public void setCategory(WordCategory category) {
        this.category = category;
    }
}
