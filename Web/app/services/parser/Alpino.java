package services.parser;

import models.NormalisedText;
import models.Settings;
import models.WordCategory;
import org.apache.commons.lang3.SystemUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import services.LogService;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.Arrays;

public class Alpino extends DependencyParser {
    private Settings settings = Settings.getInstance();

    @Override
    public String parseText(String text) {
        text = text.replaceAll("[^a-zA-Z0-9 ]", "");
        LogService.getInstance().info("Verwerken van de tekst: " + text);

        if (SystemUtils.IS_OS_WINDOWS) {
            callExternalProgram(settings.getALPINO_PATH(), Arrays.asList("cmd", "/c", String.format("runner.bat \"%s\"", text)));
        } else {
            try {
                String command = "sh " + settings.getALPINO_PATH() + "/runner.sh \"" + text + "\"";
                LogService.getInstance().info("Commando: " + command);
                Process p = Runtime.getRuntime().exec(new String[]{"bash", "-c", command});
                p.waitFor();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    public NormalisedText getOutPut() {
        try {
            File fXmlFile = new File(settings.getALPINO_PATH() + "/tmp/1.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();

            // Zin ophalen
            NodeList sentenceList = doc.getElementsByTagName("sentence");

            if (sentenceList.getLength() > 0) {
                NormalisedText nt = new NormalisedText(sentenceList.item(0).getTextContent());

                // Nodes doorlopen
                NodeList nList = doc.getElementsByTagName("node");

                if (nList == null || nList.getLength() <= 0)
                    throw new Exception("Parsen mislukt");

                Node root = nList.item(0);
                WordCategory category = getNodes(root, null);

                nt.setMainCategory(category);

                return nt;
            }
        } catch (Exception ex) {
            LogService.getInstance().error(ex.getMessage());
            ex.printStackTrace();
        }

        return null;
    }

    private WordCategory getNodes(Node root, WordCategory category) {
        for (int i = 0; i < root.getChildNodes().getLength(); i++) {
            Node node = root.getChildNodes().item(i);

            if (node.hasChildNodes()) {
                if (node.getAttributes().getNamedItem("cat") != null) {
                    if (category == null) {
                        category = new WordCategory(null, node.getAttributes().getNamedItem("cat").getNodeValue());
                    } else {
                        category = category.addCategory(node.getAttributes().getNamedItem("cat").getNodeValue());
                    }
                }

                getNodes(node, category);
            } else {
                if (category == null && node.getAttributes() != null && node.getAttributes().getNamedItem("cat") != null) {
                    category = new WordCategory(null, node.getAttributes().getNamedItem("cat").getNodeValue());
                    getNodes(node, category);
                }
            }

            if (node.getAttributes() != null) {
                // Zodra iets niet kan worden verwerkt door Alpino, dan slaan wij het ook over
                if (node.getAttributes().getNamedItem("pos") == null ||
                    node.getAttributes().getNamedItem("rel") == null ||
                    node.getAttributes().getNamedItem("root") == null ||
                    category == null) {
                        continue;
                }

                category.addWord(
                    node.getAttributes().getNamedItem("pos").getNodeValue(),
                    node.getAttributes().getNamedItem("rel").getNodeValue(),
                    node.getAttributes().getNamedItem("root").getNodeValue()
                );
            }
        }

        return category;
    }
}
