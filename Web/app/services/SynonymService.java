package services;

import com.fasterxml.jackson.databind.JsonNode;
import models.Word;

import java.util.ArrayList;

public class SynonymService {
	public ArrayList<Word> getWords() {
		ArrayList<Word> words = new ArrayList<>();

		JsonNode json = GraphDBService.execute("MATCH (w:Word)\n"
				+ "WHERE w.pos = 'name' OR w.pos = 'noun'\n" + "WITH w\n"
				+ "OPTIONAL MATCH (s:Synonym)-[SYNONYM_OF]->(w:Word)\n"
				+ "RETURN DISTINCT w.root, w.pos, w.rel, id(w), s.value\n"
				+ "ORDER BY w.root ASC");

		Word word;
		for (JsonNode item : json.get("data")) {
			word = new Word(item.get(0).asText(), item.get(1).asText(), item
					.get(2).asText(), item.get(3).asInt());

			if (words.contains(word)) {
				word = words.get(words.indexOf(word));
			}

			if (!item.get(4).isNull()) {
				word.addSynonym(item.get(4).asText());
			}

			if (words.contains(word)) {
				words.set(words.indexOf(word), word);
			} else {
				words.add(word);
			}
		}

		return words;
	}
	
	public ArrayList<Word> getWord(long id){
		ArrayList<Word> words = new ArrayList<>();
		String strId = Long.toString(id);
		
		JsonNode json = GraphDBService.execute(String
				.format("MATCH (w:Word)\n"
				+ "WHERE id(w)= %s \n" 
				+ "WITH w\n"
				+ "OPTIONAL MATCH (s:Synonym)-[SYNONYM_OF]->(w:Word)\n"
				+ "RETURN DISTINCT w.root, w.pos, w.rel, id(w), s.value\n"
				+ "ORDER BY w.root ASC", strId));
		
		Word word;
		for (JsonNode item : json.get("data")) {
			word = new Word(item.get(0).asText(), item.get(1).asText(), item
					.get(2).asText(), item.get(3).asInt());

			if (words.contains(word)) {
				word = words.get(words.indexOf(word));
			}

			if (!item.get(4).isNull()) {
				word.addSynonym(item.get(4).asText());
			}

			if (words.contains(word)) {
				words.set(words.indexOf(word), word);
			} else {
				words.add(word);
			}
		}

		return words;
		
		
	}

	public void RemoveWord(long id) {
		/*
		 * MATCH (w:Word) WHERE id(w) = %s OPTIONAL MATCH
		 * (s:Synonym)-[SYNONYM_OF]->(w:Word) DETACH DELETE w
		 */

		String strId = Long.toString(id);
		
		JsonNode json = GraphDBService
				.execute(String
						.format("MATCH (w:Word) \n " 
								+ "WHERE id(w) = %s \n " 
								+ "OPTIONAL MATCH(s:Synonym)-[SYNONYM_OF]->(w:Word)\n "
								+ "DETACH DELETE w ", strId)
				);
		
	}

	public Word getById(long id) {
		JsonNode json = GraphDBService.execute(String.format(
				"MATCH (w:Word) WHERE id(w) = %s \n"
						+ "OPTIONAL MATCH (s:Synonym)-[SYNONYM_OF]->(w:Word)\n"
						+ "RETURN w.root, w.pos, w.rel, s.value;", id));

		if (json.get("data").isNull()) {
			return null;
		}

		Word word = new Word(json.get("data").get(0).get(0).asText(), json
				.get("data").get(0).get(1).asText(), json.get("data").get(0)
				.get(2).asText(), id);

		for (JsonNode node : json.get("data")) {
			if (!node.get(3).isNull())
				word.addSynonym(node.get(3).asText());
		}

		return word;
	}

	public void addSynonym(long wordid, String synonym) {
		GraphDBService
				.execute(String
						.format("MATCH (w:Word) WHERE id(w) = %s \n"
								+ "MERGE (s:Synonym { value : '%s' })-[r:SYNONYM_OF]->(w) \n"
								+ "RETURN w.root, w.pos, w.rel;", wordid,
								synonym));
	}

	public void delete(Long wordId, String item) {
		GraphDBService.execute(String.format(
				"MATCH (s:Synonym)-[r:SYNONYM_OF]->(w:Word)\n"
						+ "WHERE id(w) = %s AND s.value = '%s'\n"
						+ "DELETE r, s;", wordId, item));
	}
}
