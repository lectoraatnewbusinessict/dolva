package services;

import com.fasterxml.jackson.databind.JsonNode;

import models.Account;

import services.GraphDBService;
import services.SecurityService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ControlPanelService {

	public void saveAnswersToDB(List<String> list) throws NullPointerException {
		ArrayList<String> array = new ArrayList<String>();
		array = SecurityService.securePassword(list.get(3));

		String password = array.get(0);
		String salt = array.get(1);

		String name = list.get(0);
		String surname = list.get(1);
		String username = list.get(2).toLowerCase();
		String date = list.get(4);
		String role = list.get(5);

		JsonNode json = GraphDBService.execute(String.format(
				"CREATE (u:Username " + "{ " + "name : '%s', " + "surname : '%2$s', " + "username : '%3$s', "
						+ "password : '%4$s', " + "salt : '%5$s'," + "date : '%6$s'," + "role : '%7$s'" + "})",
				name, surname, username, password, salt, date, role));
	}

	public ArrayList<Account> getAccounts() {

		JsonNode node = GraphDBService.execute("MATCH (n:Username)" +

		"RETURN DISTINCT n.name, n.surname, n.date, n.role, n.username;");

		return getUsers(node);
	}

	public ArrayList<Account> getAccount(String username) {
		
		JsonNode node = GraphDBService.execute(String.format
				(
						"MATCH (n:Username)" +
						"WHERE n.username = '"+ username +"'" +
						"RETURN DISTINCT n.name, n.surname, n.date, n.role, n.username;"
				));
		
		return getUsers(node);
	}

	public ArrayList<Account> getUsers(JsonNode node) {
		ArrayList<Account> accounts = new ArrayList<>();
		for (JsonNode item : node.get("data")) {
			accounts.add(new Account(item.get(0).asText(), // name
					item.get(1).asText(), // surname
					item.get(2).asText(), // date of creation
					item.get(3).asText(), // role/function of user
					item.get(4).asText() // Username of user
			));
		}
		return accounts;
	}
	
	
}