package services;

import com.fasterxml.jackson.databind.JsonNode;

import java.math.BigInteger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import services.GraphDBService;

import java.util.ArrayList;
import java.util.HashMap;	
import java.util.List;
import java.util.Map;

import java.lang.StringBuilder;

public class SecurityService {
	private static int ITERATIONS = 1000;

	/*
	 * @param String passwordToHash;
	 * 
	 * @return ArrayList<String> [String generatedPassword],[String salt]
	 * 
	 * This method returns an arraylist with two strings, the password and salt.
	 * The salt is randomly generated.
	 */

	public static ArrayList<String> securePassword(String passwordToHash) {
		ArrayList<String> secure = new ArrayList<String>();
		String generatedPassword = null;
		String salt = null;
		try {
			salt = getSalt();
			generatedPassword = hashPassword(passwordToHash, salt);
			secure.add(generatedPassword);
			secure.add(salt);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return secure;
	}

	public static String getSalt() throws NoSuchAlgorithmException {
		// Always use a SecureRandom generator
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		// Create array for salt
		byte[] salt = new byte[16];
		// Get a random salt
		sr.nextBytes(salt);
		// return salt
		return salt.toString();
	}

	/*
	 * @param String username, String password;
	 * 
	 * @return boolean true/false
	 * 
	 * This method auhenticates a user and returns either true (authenticated)
	 * or false (authentication failure.)
	 */
	
	public static boolean AuthenticateUser(String username, String passwordToHash) {
		boolean auth = false;
		String passwordInDB = "";
		String salt = "";
		
		JsonNode node = GraphDBService.execute(String.format("match (n:Username) WHERE n.username = '" + username.toLowerCase() + "' return n.password, n.salt;"));
			
		for (JsonNode item : node.get("data")) {
			passwordInDB = item.get(0).asText();
			salt = item.get(1).asText();
		}
		
			String hashedPassword = hashPassword(passwordToHash, salt);
			//String hashedPassword = "asdf";
			if(hashedPassword != null && !hashedPassword.isEmpty()){
				if(hashedPassword.equals(passwordInDB)) { 	
					auth = true;
				}else{
					auth = false;
				}
			}	
		return auth;
	}

	/*
	 * @param: String: passwordToHash, String salt;
	 * 
	 * @return: String: returnString;
	 * 
	 * This method returns a secure hashed password.
	 */
	public static String hashPassword(String passwordToHash, String salt) {
		String returnString = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.reset();
			md.update(salt.getBytes());
			byte[] bytes = md.digest(passwordToHash.getBytes());

			// slowness factor, increase/decrease ITERATIONS based on server
			// performance.
			for (int a = 0; a < ITERATIONS; a++) {
				md.reset();
				bytes = md.digest(bytes);
			}

			StringBuilder sb = new StringBuilder();

			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			returnString = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return returnString;
	}
}