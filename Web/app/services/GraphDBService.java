package services;

import com.fasterxml.jackson.databind.JsonNode;
import models.Neo4jQuery;
import models.Settings;
import play.libs.F;
import play.libs.ws.WS;
import play.libs.ws.WSResponse;

public class GraphDBService {

    private static final Settings SETTINGS = Settings.getInstance();

    public static JsonNode execute(String query) {
        LogService.getInstance().info("Query uitvoeren: " /* String.format("%s/db/data/cypher", Settings.getNEO4J_SERVER()) + " : "*/ + query);
        Neo4jQuery neo4jQuery = new Neo4jQuery(query);

        F.Promise<JsonNode> jsonPromise =
                WS.url(String.format("%s/db/data/cypher", SETTINGS.getNEO4J_SERVER()))
                        .setHeader("Authorization", SETTINGS.getNEO4J_AUTH())
                        .post(neo4jQuery.toJson()).map(WSResponse::asJson);

        JsonNode node = jsonPromise.get(10000);
        LogService.getInstance().info(node.toString());
        return node;
    }

}
