package services;

import com.fasterxml.jackson.databind.JsonNode;
import models.Answer;
import models.NormalisedText;
import models.Word;
import models.WordCategory;
import services.parser.Alpino;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * TODO Improvement use object layer of neo4j?
 */

public class QuestionService {
	public void saveAnswersToDB(String category, List<NormalisedText> answers)
			throws NullPointerException {
		JsonNode json = GraphDBService.execute(String.format(
				"MERGE (n:Category { name : '%s' }) RETURN id(n);", category));

		if (json.get("data") == null || json.get("data").get(0) == null
				|| json.get("data").get(0).get(0) == null) {
			throw new NullPointerException("Node is niet met succes aangemaakt");
		}

		String id = json.get("data").get(0).get(0).toString();

		// Antwoorden vullen
		for (NormalisedText answer : answers) {
			json = GraphDBService
					.execute(String
							.format("MATCH (q:Category) WHERE id(q) = %2$s\n"
									+ "MERGE (a:Answer { sentence : '%s', parent: %s })\n"
									+ "CREATE UNIQUE a-[:ANSWER_OF_CATEGORY]->q\n"
									+ "RETURN id(a);", answer.toString(), id));

			addAnswerCategories(json.get("data").get(0).get(0).toString(),
					null, answer.getMainCategory());
		}
	}

	// nieuwe methode: Moet de antwoorden of categorie wijzigen

	public ArrayList<Answer> getAnswer(long id) {
		String strId = Long.toString(id);

		ArrayList<Answer> answers = new ArrayList<>();

		JsonNode node = GraphDBService.execute(String.format(
				"MATCH (n:Answer)-[:ANSWER_OF_CATEGORY]->(c:Category)\n"
						+ "WHERE id(n) = %s \n"
						+ "RETURN DISTINCT n.sentence, c.name, id(n)", strId));

		for (JsonNode item : node.get("data")) {
			answers.add(new Answer(item.get(0).asText(), item.get(1).asText(),
					item.get(2).asInt()));
		}
		return answers;
	}

	public void changeAnswersInDB(long id, String category,
			List<NormalisedText> answers) throws NullPointerException {
		String strId = Long.toString(id);
		NormalisedText answer = answers.get(0);

		JsonNode node = GraphDBService.execute(String.format(
				"MATCH (a:Answer)-[:ANSWER_OF_CATEGORY]->(c:Category)\n "
						+ "WHERE id(a)= %s \n" + "SET c.name = '%2$s' \n "
						+ "SET a.sentence = '%3$s' ", strId, category,
				answer.toString()));
	}

	public void DeleteAnswerFormDB(long id){
		String strId = Long.toString(id);
		
				JsonNode node = GraphDBService.execute(String.format(
						"MATCH (a:Answer)-[:ANSWER_OF_CATEGORY]->(c:Category)\n "
								+ " WHERE id(a)= %s \n "
								+ " DETACH DELETE a ", strId));
		
	}

	public ArrayList<Answer> getAnswers() {
		ArrayList<Answer> answers = new ArrayList<>();

		JsonNode node = GraphDBService
				.execute("MATCH (n:Answer)-[:ANSWER_OF_CATEGORY]->(c:Category) RETURN DISTINCT n.sentence, c.name, id(n)");

		for (JsonNode item : node.get("data")) {
			answers.add(new Answer(item.get(0).asText(), item.get(1).asText(),
					item.get(2).asInt()));
		}

		return answers;
	}

	public Answer getById(long id) {
		JsonNode json = GraphDBService.execute(String.format(
				"MATCH (n:Answer)-[:ANSWER_OF_CATEGORY]->(c:Category) WHERE id(n) = %s \n"
						+ "RETURN n.sentence, c.name, id(n)", id));

		if (json.get("data").isNull()) {
			return null;
		}

		return new Answer(json.get(0).asText(), json.get(1).asText(), json.get(
				2).asInt());
	}

	public String askQuestion(String question) {
		LogService.getInstance().info("Vraag gesteld:" + question);

		String answer = "Ik begrijp u niet, zou u uw vraag anders kunnen formuleren?";
		Alpino parser = new Alpino();
		parser.parseText(question);
		NormalisedText text = parser.getOutPut();

		ArrayList<Word> wordsToSearch = text.getNouns(text.getMainCategory());
		wordsToSearch.addAll(text.getVerbs(text.getMainCategory()));
		wordsToSearch.addAll(text.getNames(text.getMainCategory()));

		if (!wordsToSearch.isEmpty()) {
			String answerOrList = ""; // "n.pos = \"noun\" OR n.pos = \"name\" AND ";
			LogService.getInstance().info(wordsToSearch.size());

			for (int i = 0; i < wordsToSearch.size(); i++) {
				Word noun = wordsToSearch.get(i);
				answerOrList += String
						.format("qw.root = '%s' ", noun.getRoot());

				if (i < wordsToSearch.size() - 1) {
					answerOrList += " OR ";
				}
			}

			String query = "MATCH (qw:Word)-[:WORD_OF|WORD_CATEGORY_OF*]->(q:Answer)\n"
					+ "WHERE \n (%s)\n"
					+ "RETURN DISTINCT q.sentence, qw.root, qw.pos, qw.rel";

			JsonNode json = GraphDBService.execute(String.format(query,
					answerOrList));

			// Parsen van de json
			HashMap<String, Word> results = new HashMap<>();

			if (!json.get("data").isNull() && json.get("data").size() > 0
					&& json.get("data").get(0).size() > 0) {
				for (JsonNode node : json.get("data")) {
					results.put(node.get(0).asText(), new Word(node.get(1)
							.asText(), node.get(2).asText(), node.get(3)
							.asText()));
				}

				if (results.size() > 0) {
					answer = results.entrySet().iterator().next().getKey();
				}

				for (Map.Entry<String, Word> result : results.entrySet()) {
					if (result.getValue().getPos().equals("noun")) {
						answer = result.getKey();
					}
				}

				LogService.getInstance().info("Antwoord gevonden: " + answer);
			}
		} else {
			LogService.getInstance().info(
					"Geen woorden gevonden om mee te zoeken.");
		}

		return answer;
	}

	private void addAnswerCategories(String id, String parent,
			WordCategory category) {
		if (parent != null) {
			GraphDBService
					.execute(String
							.format("MATCH (a:Answer) WHERE id(a) = %s\n"
									+ "MATCH (p:Wordcategory { name:  '%s', parent: '%1$s' })\n"
									+ "MERGE (c:Wordcategory { name : '%s', parent: '%1$s' })\n"
									+ "CREATE UNIQUE c-[:WORD_CATEGORY_OF]->p;",
									id, parent, category.getName()));
		} else {
			GraphDBService
					.execute(String
							.format("MATCH (a:Answer) WHERE id(a) = %s\n"
									+ "MERGE (n:Wordcategory { name : '%s', parent: '%1$s' })\n"
									+ "CREATE UNIQUE n-[:WORD_CATEGORY_OF]->a;",
									id, category.getName()));
		}

		for (Word word : category.getWords()) {
			GraphDBService
					.execute(String
							.format("MATCH (a:Answer) WHERE id(a) = %s\n"
									+ "MATCH (c:Wordcategory {name: '%s', parent: '%1$s'})\n"
									+ "MERGE (w:Word { pos: '%s', rel: '%s', root:'%s', parent: '%1$s'  })\n"
									+ "CREATE UNIQUE w-[:WORD_OF]->c;", id,
									category.getName(), word.getPos(),
									word.getRel(), word.getRoot()));
		}

		if (category.hasCategories()) {
			for (WordCategory item : category.getCategories()) {
				addAnswerCategories(id, category.getName(), item);
			}
		}
	}
}
