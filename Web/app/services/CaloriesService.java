package services;

import com.fasterxml.jackson.databind.JsonNode;
import models.Product; 
import models.NormalisedText;
import models.Word;
import models.WordCategory;
import services.parser.Alpino;
 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * TODO Improvement use object layer of neo4j?
 */

public class CaloriesService {
	 

	public void saveProductsToDB(List<NormalisedText> products)
			throws NullPointerException {

		NormalisedText productname = products.get(0);
		NormalisedText eenheid = products.get(1);
		NormalisedText kcal = products.get(2);
		NormalisedText kJ = products.get(3);

		JsonNode json = GraphDBService
				.execute(String
						.format(" CREATE (p:Product { \n " + 
								" product: '%s', \n " + 
								" unity: '%2$s', \n " + 
								" kcal: '%3$s', \n " + 
								" kJ: '%4$s'})",
								productname.toString(), eenheid.toString(),
								kcal.toString(), kJ.toString())
				);
	}
	
	
	
	public ArrayList<Product> getProducts() {
        ArrayList<Product> products = new ArrayList<>();

        JsonNode node = GraphDBService.execute("MATCH (p:Product) RETURN p.product, p.unity, p.kcal, p.kJ, id(p)");

        for (JsonNode item : node.get("data")) {
            products.add(new Product(item.get(0).asText(), item.get(1).asText(), item.get(2).asText(), item.get(3).asText(), item.get(4).asInt()));
        }


        return products;
    }
	

	
	public ArrayList<Product> getProduct(long id){
		String strId = Long.toString(id);
		ArrayList<Product> products = new ArrayList<>();
		
		JsonNode node = GraphDBService.execute(String.format("MATCH (p:Product) \n " +
															"WHERE id(p)= %s \n" + 
															"RETURN p.product, p.unity, p.kcal, p.kJ, id(p)", strId));
		

        for (JsonNode item : node.get("data")) {
            products.add(new Product(item.get(0).asText(), item.get(1).asText(), item.get(2).asText(), item.get(3).asText(), item.get(4).asInt()));
        }
				
						
		return products; 
	}
	
//	public void changeUserInDB(long id, List<NormalisedText> users){
//		System.out.println(users);
//		String strId = Long.toString(id);
//		NormalisedText naam = users.get(0);
//		NormalisedText achternaam = users.get(1);
//		NormalisedText username = users.get(2);
//		NormalisedText password = users.get(3);
//		String wachtwoord = password.toString();
//		String w8woord = md5(wachtwoord);
//		NormalisedText functie = users.get(5);
//		String function = functie.toString();
//		
//		JsonNode node = GraphDBService.execute(String.format(" MATCH (n:Username)\n "+ 
//				"WHERE id(n) = %s \n" +
//				"SET n.name = '%2$s'\n" +
//				"SET n.surname = '%3$s'\n" +
//				"SET n.username = '%4$s'\n" +
//				"SET n.password = '%5$s'\n" +
//				"SET n.function = '%6$s'", strId, naam.toString(), achternaam.toString(), username.toString(), w8woord , function));
//		//, strId, naam.toString(), achternaam.toString(), username.toString(), wachtwoord, functie.toString()
//						
//	}
	
	// hoi 
	
}