package services;

import com.fasterxml.jackson.databind.JsonNode;
import models.Answer;
import models.Meal;
import models.NormalisedText;
import models.Word;
import models.WordCategory;
import services.parser.Alpino;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * TODO Improvement use object layer of neo4j?
 */

public class TestService {

	public void saveAnswersToDB(List<NormalisedText> meals)
			throws NullPointerException {

		NormalisedText datum = meals.get(0);
		NormalisedText maaltijd = meals.get(1);
		NormalisedText calorien = meals.get(2);

		JsonNode json = GraphDBService
				.execute(String
						.format("MERGE (d:Date {datum:'%s'})\n "
								+ "MERGE (m:Meal {maaltijd:'%2$s'})\n"
								+ "MERGE (c:Calories {Calorien:'%3$s'})\n"
								+ "CREATE (d)-[:DATE_OF_MEAL]->(m)-[:CALORIES_OF_MEAL]->(c)",
								datum.toString(), maaltijd.toString(), calorien.toString()));

	}

	public ArrayList<Meal> getMeals() {
		ArrayList<Meal> meals = new ArrayList<>();

		JsonNode node = GraphDBService
				.execute("MATCH (d:Date)-[:DATE_OF_MEAL]->(m:Meal)-[:CALORIES_OF_MEAL]->(c:Calories) RETURN DISTINCT d.datum,m.maaltijd,c.Calorien");

		for (JsonNode item : node.get("data")) {
			meals.add(new Meal(item.get(0).asText(), item.get(1).asText(), item
					.get(2).asText()));
		}

		return meals;
	}

}