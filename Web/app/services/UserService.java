package services;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import com.fasterxml.jackson.databind.JsonNode;
import models.Answer;
import models.User;
import models.NormalisedText;
import models.Word;
import models.WordCategory;
import services.parser.Alpino;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * TODO Improvement use object layer of neo4j?
 */

public class UserService {

	public void saveUsersToDB(List<NormalisedText> users)
			throws NullPointerException {

		NormalisedText naam = users.get(0);
		NormalisedText achternaam = users.get(1);
		NormalisedText username = users.get(2);
		NormalisedText password = users.get(3);
		String wachtwoord = password.toString();
		wachtwoord = md5(wachtwoord);
		NormalisedText datum = users.get(4);
		NormalisedText functie = users.get(5);
				
		// NormalisedText maaltijd = meals.get(1);
		// NormalisedText calorien = meals.get(2);
		
		JsonNode json = GraphDBService
				.execute(String
						.format("CREATE (u:Username {\n" 
								+"name: '%s', \n"
								+ "surname: '%2$s', \n"
								+ "username: '%3$s', \n"
								+ "password: '%4$s', \n"
								+ "date: '%5$s', \n"
								+ "function: '%6$s'})", naam.toString(), achternaam.toString(), username.toString(), wachtwoord, datum.toString(), functie.toString()));

}
	


	public ArrayList<User> getUsers() {
		ArrayList<User> users = new ArrayList<>();

		JsonNode node = GraphDBService
				.execute("MATCH (n:Username) \n "
						+ " RETURN n.name, n.surname, n.username, n.password, n.date, n.function, id(n)");
		
		
		

		for (JsonNode item : node.get("data")) {
			users.add(new User(item.get(0).asText(), item.get(1).asText(), item
					.get(2).asText(), item.get(3).asText(), item.get(4)
					.asText(), item.get(5)
					.asText(), item.get(6)
					.asInt()));
		}
		//
		return users;
	}
	
	public ArrayList<User> getUser(long id){
		String strId = Long.toString(id);
		
		ArrayList<User> users = new ArrayList<>();

		JsonNode node = GraphDBService
				.execute(String.format("MATCH (n:Username) \n "
						+ "WHERE id(n) = %s \n"
						+ " RETURN n.name, n.surname, n.username, n.password, n.date, n.function, id(n)", strId));
		
		
		

		for (JsonNode item : node.get("data")) {
			users.add(new User(item.get(0).asText(), item.get(1).asText(), item
					.get(2).asText(), item.get(3).asText(), item.get(4)
					.asText(), item.get(5)
					.asText(), item.get(6)
					.asInt()));
		}
		//
		return users;
			
	}
	
	
	public static String md5(String input){
		String md5 = "hoi";
		  try {
	             
		        //Create MessageDigest object for MD5
		        MessageDigest digest = MessageDigest.getInstance("MD5");
		         
		        //Update input string in message digest
		        digest.update(input.getBytes(), 0, input.length());
		 
		        //Converts message digest value in base 16 (hex) 
		        md5 = new BigInteger(1, digest.digest()).toString(16);
		 
		        } catch (NoSuchAlgorithmException e) {
		 
		            e.printStackTrace();
		        }
		
		return md5;
	}
	
	public void deleteUserFromDB(long id){
		String strId = Long.toString(id);
		
		JsonNode node = GraphDBService
				.execute(String.format("MATCH (n:Username) \n "
						+ "WHERE id(n) = %s \n"
						+ "DETACH DELETE n \n", strId));
		
		
//				JsonNode node = GraphDBService.execute(String.format(
//						"MATCH (a:Answer)-[:ANSWER_OF_CATEGORY]->(c:Category)\n "
//								+ " WHERE id(a)= %s \n "
//								+ " DETACH DELETE a ", strId));
		
	}
	
	public void changeUserInDB(long id, List<NormalisedText> users){
		System.out.println(users);
		String strId = Long.toString(id);
		NormalisedText naam = users.get(0);
		NormalisedText achternaam = users.get(1);
		NormalisedText username = users.get(2);
		NormalisedText password = users.get(3);
		String wachtwoord = password.toString();
		String w8woord = md5(wachtwoord);
		NormalisedText functie = users.get(5);
		String function = functie.toString();
		
		JsonNode node = GraphDBService.execute(String.format(" MATCH (n:Username)\n "+ 
				"WHERE id(n) = %s \n" +
				"SET n.name = '%2$s'\n" +
				"SET n.surname = '%3$s'\n" +
				"SET n.username = '%4$s'\n" +
				"SET n.password = '%5$s'\n" +
				"SET n.function = '%6$s'", strId, naam.toString(), achternaam.toString(), username.toString(), w8woord , function));
		//, strId, naam.toString(), achternaam.toString(), username.toString(), wachtwoord, functie.toString()
						
	}
	

}