package services;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;

public class LogService {
    public static LogService _instance = null;
    private ArrayList<String> log;

    protected LogService() {
        this.log = new ArrayList<String>();
    }

    public static LogService getInstance() {
        if (_instance == null) {
            _instance = new LogService();
        }

        return _instance;
    }

    public void addToLog(String txt) {
        String msg = String.format("%tc - %s", new Date(), txt);

        log.add(msg);
        System.out.println(msg);
    }

    public void error(String txt) {
        addToLog("[error] " + txt);
    }

    public void info(String txt) {
        addToLog("[info] " + txt);
    }

    public void info(int txt) {
        addToLog("[info] " + txt);
    }

    @Override
    public String toString() {
        StringWriter writer = new StringWriter();

        log.forEach((v) -> {
            writer.append(String.format("%s\n", v));
        });

        return writer.toString();
    }
}
