package services;

import com.fasterxml.jackson.databind.JsonNode;
import models.diary.DiaryMetaModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Service for retrieval of diary information.
 */
public class DiaryService {

    private static final String[] PROPERTIES = new String[] {"name", "date"};

    /**
     * Gets the meta information from all found diary entries.
     *
     * @return A List containing meta information from all the found diary entries.
     */
    public List<DiaryMetaModel> getMetadatas() {
        StringBuilder query = new StringBuilder("MATCH (m:" + DiaryMetaModel.TYPE + ")\n");
        query.append("RETURN DISTINCT id(m)");
        for (String p : PROPERTIES) {
            query.append(", m.").append(p);
        }
        query.append("\n");
        query.append("ORDER BY m.date DESC");

        JsonNode data = GraphDBService.execute(query.toString()).get("data");

        List<DiaryMetaModel> metas = new ArrayList<>();
        DiaryMetaModel meta;
        for (JsonNode row : data) {
            meta = new DiaryMetaModel();

            meta.setId(row.get(0).asInt());
            for (int i = 0; i < PROPERTIES.length; i++) {
                meta.addProperty(PROPERTIES[i], row.get(i + 1).asText());
            }

            metas.add(meta);
        }

        return metas;
    }

    /**
     * Gets the meta information of diary.
     *
     * @param id The id of the meta model.
     * @return The meta information
     */
    public DiaryMetaModel getMetadataById(long id) {
        StringBuilder query = new StringBuilder("MATCH (m:").append(DiaryMetaModel.TYPE).append(")\n");
        query.append("WHERE id(m) = ").append(id).append("\n");
        query.append("RETURN DISTINCT id(m)");
        for (String p : PROPERTIES) {
            query.append(", m.").append(p);
        }

        JsonNode data = GraphDBService.execute(query.toString()).get("data");

        if (data.size() < 1) {
            return null;
        }
        data = data.get(0);

        DiaryMetaModel meta = new DiaryMetaModel();
        meta.setId(data.get(0).asInt());
        for (int i = 0; i < PROPERTIES.length; i++) {
            meta.addProperty(PROPERTIES[i], data.get(i + 1).asText());
        }

        return meta;
    }

    /**
     * Gets the comments of the diary.
     *
     * @param id The id of the diary.
     * @return A JsonNode containing the comments of the diary.
     */
    public JsonNode getComments(long id) {
        StringBuilder query = new StringBuilder("MATCH (m:").append(DiaryMetaModel.TYPE).append(")");
        query.append("-[:COMMENT]->(c)\n");
        query.append("WHERE id(m) = ").append(id).append("\n");
        query.append("RETURN collect(c.text)");

        return this.getQueryResult(query.toString());
    }

    /**
     * Gets the feelings.
     *
     * @param id The id of the diary.
     * @return A JsonNode containing the feelings of the diary.
     */
    public JsonNode getFeelings(long id) {
        StringBuilder query = new StringBuilder("MATCH (m:").append(DiaryMetaModel.TYPE).append(")");
        query.append("-[:ENTRY]->(e)-[:QUESTION]->(q)-[:ANSWER]->(a)\n");
        query.append("WHERE id(m) = ").append(id).append(" AND e.text = 'feelings'\n");
        query.append("RETURN {question: q.text, answer: a.text}");

        return this.getQueryResult(query.toString());
    }

    /**
     * Gets the obstacles diary.
     *
     * @param id The id of the diary.
     * @return A JsonNode containing the obstacles of the diary.
     */
    public JsonNode getObstacles(long id) {
        StringBuilder query = new StringBuilder("MATCH (m:").append(DiaryMetaModel.TYPE).append(")");
        query.append("-[:ENTRY]->(e)-[:QUESTION]->(q)-[:ANSWER]->(a)\n");
        query.append("WHERE id(m) = ").append(id).append(" AND e.text = 'obstacles'\n");
        query.append("RETURN {question: q.text, answer: collect({text: a.text, intensity: TOINT(a.intensity)})}");

        return this.getQueryResult(query.toString());
    }

    /**
     * Gets the irrelevant behaviour.
     *
     * @param id The id of the diary.
     * @return A JsonNode containing the irrelevant behaviour of the diary.
     */
    public JsonNode getIrrelevantBehavior(long id) {
        StringBuilder query = new StringBuilder("MATCH (m:").append(DiaryMetaModel.TYPE).append(")");
        query.append("-[:ENTRY]->(e)-[:QUESTION]->(q)-[:ANSWER]->(a)\n");
        query.append("WHERE id(m) = ").append(id).append(" AND e.text = 'irrelevantBehavior'\n");
        query.append("OPTIONAL MATCH (a)-[:EXPLANATION]->(ex)\n");
        query.append("RETURN CASE ex.text WHEN NULL THEN {question: q.text, answer: a.text} ELSE {question: q.text, answer: a.text, explanation: ex.text} END");

        return this.getQueryResult(query.toString());
    }

    /**
     * Executes and returns the data of the given query. It is assumed that the query returns just one column,
     * as that is the only place it will look for data.
     *
     * @param query The query that needs to be executed.
     * @return A JsonNode containing the data of the query.
     */
    private JsonNode getQueryResult(String query) {
        // Get the first column of the first result (there should be only one of either).
        JsonNode result = GraphDBService.execute(query).get("data").get(0);
        if (result != null) {
            return result.get(0);
        }
        return null;
    }

}
