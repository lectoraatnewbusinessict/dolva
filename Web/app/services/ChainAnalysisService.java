package services;

import com.fasterxml.jackson.databind.JsonNode;
import models.chain.ChainAnalysisMetaModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Service for retrieval of parts of a chain entry.
 */
public class ChainAnalysisService {

    private static final String[] PROPERTIES = new String[] {"name", "analysisDate", "behaviorDate"};

    /**
     * Gets the meta information from all found chain entries.
     *
     * @return A List containing meta information from all the found chain entries.
     */
    public List<ChainAnalysisMetaModel> getMetadatas() {
        StringBuilder query = new StringBuilder("MATCH (m:" + ChainAnalysisMetaModel.TYPE + ")\n");
        query.append("RETURN DISTINCT id(m)");
        for (String p : PROPERTIES) {
            query.append(", m.").append(p);
        }
        query.append("\n");
        query.append("ORDER BY m.analysisDate DESC");

        JsonNode data = GraphDBService.execute(query.toString()).get("data");

        List<ChainAnalysisMetaModel> metas = new ArrayList<>();
        ChainAnalysisMetaModel meta;
        for (JsonNode row : data) {
            meta = new ChainAnalysisMetaModel();

            meta.setId(row.get(0).asInt());
            for (int i = 0; i < PROPERTIES.length; i++) {
                meta.addProperty(PROPERTIES[i], row.get(i + 1).asText());
            }

            metas.add(meta);
        }

        return metas;
    }

    /**
     * Gets the meta information of chain analysis.
     *
     * @param id The id of the meta model.
     * @return The meta information
     */
    public ChainAnalysisMetaModel getMetadataById(long id) {
        StringBuilder query = new StringBuilder("MATCH (m:").append(ChainAnalysisMetaModel.TYPE).append(")\n");
        query.append("WHERE id(m) = ").append(id).append("\n");
        query.append("RETURN DISTINCT id(m)");
        for (String p : PROPERTIES) {
            query.append(", m.").append(p);
        }

        JsonNode data = GraphDBService.execute(query.toString()).get("data");

        if (data.size() < 1) {
            return null;
        }
        data = data.get(0);

        ChainAnalysisMetaModel meta = new ChainAnalysisMetaModel();
        meta.setId(data.get(0).asInt());
        for (int i = 0; i < PROPERTIES.length; i++) {
            meta.addProperty(PROPERTIES[i], data.get(i + 1).asText());
        }

        return meta;
    }

    /**
     * Gets the problem behaviour.
     *
     * @param id The id of the chain analysis.
     * @return A JsonNode containing the information about the problem behaviour.
     */
    public JsonNode getProblemBehavior(long id) {
        StringBuilder query = new StringBuilder("MATCH (m:").append(ChainAnalysisMetaModel.TYPE).append(")");
        query.append("-[:STEP]->(s)-[:QUESTION]->(q)-[:ANSWER]->(a)\n");
        query.append("WHERE id(m) = ").append(id).append(" AND s.text = 'problemBehavior'\n");
        query.append("OPTIONAL MATCH (a)-[:MORE*]->(am)\n");
        query.append("OPTIONAL MATCH (a)-[:FOOD]->(f)\n");
        query.append("RETURN {question: q.text, answer: [a.text] + collect(DISTINCT am.text), food: collect(DISTINCT f.text)}");

        return this.getQueryResult(query.toString());
    }

    /**
     * Gets the provocation.
     *
     * @param id The id of the chain analysis.
     * @return A JsonNode containing the information about the provocation.
     */
    public JsonNode getProvocation(long id) {
        StringBuilder query = new StringBuilder("MATCH (m:").append(ChainAnalysisMetaModel.TYPE).append(")");
        query.append("-[:STEP]->(s)-[:QUESTION]->(q)-[:ANSWER]->(a)\n");
        query.append("WHERE id(m) = ").append(id).append(" AND s.text = 'provocation'\n");
        query.append("RETURN {question: q.text, answer: a.text}");

        return this.getQueryResult(query.toString());
    }

    /**
     * Gets the vulnerability factors.
     *
     * @param id The id of the chain analysis.
     * @return A JsonNode containing the information about the vulnerability factors.
     */
    public JsonNode getVulnerabilityFactors(long id) {
        StringBuilder query = new StringBuilder("MATCH (m:").append(ChainAnalysisMetaModel.TYPE).append(")");
        query.append("-[:STEP]->(s)-[:QUESTION]->(q)-[:CATEGORY]->(c)-[:FACTOR]->(f)\n");
        query.append("WHERE id(m) = ").append(id).append(" AND s.text = 'vulnerabilityFactors'\n");
        query.append("WITH q, {category: c.text, factors: collect({text: f.text, importance: TOINT(f.importance)})} AS answers\n");
        query.append("RETURN {question: q.text, answer: collect(answers)}");

        return this.getQueryResult(query.toString());
    }

    /**
     * Gets the chain of events.
     *
     * @param id The id of the chain analysis.
     * @return A JsonNode containing the information about the chain of events.
     */
    public JsonNode getEventChain(long id) {
        StringBuilder query = new StringBuilder("MATCH (m:").append(ChainAnalysisMetaModel.TYPE).append(")");
        query.append("-[:STEP]->(s)-[:QUESTION]-(q)-[:EVENT_CHAIN*]->(ec)\n");
        query.append("WHERE id(m) = ").append(id).append(" AND s.text = 'eventChain'\n");
        query.append("OPTIONAL MATCH (ec)-[:PREVENTION]->(p)\n");
        query.append("WITH q, CASE p.text WHEN NULL THEN {type: ec.type, text: ec.text} ELSE {type: ec.type, text: ec.text, prevention: p.text} END AS answers\n");
        query.append("RETURN {question: q.text, answer: collect(answers)}");

        return this.getQueryResult(query.toString());
    }


    /**
     * Gets the consequences.
     *
     * @param id The id of the chain analysis.
     * @return A JsonNode containing the information about the consequences.
     */
    public JsonNode getConsequences(long id) {
        StringBuilder query = new StringBuilder("MATCH (m:").append(ChainAnalysisMetaModel.TYPE).append(")");
        query.append("-[:STEP]->(s)-[:QUESTION]-(q)-[:ANSWER]->(a)\n");
        query.append("WHERE id(m) = ").append(id).append(" AND s.text = 'consequences'\n");
        query.append("OPTIONAL MATCH (a)-[:REPAIR]->(r)\n");
        query.append("WITH q, CASE r.text WHEN NULL THEN {text: a.text} ELSE {text: a.text, repair: r.text} END AS answers\n");
        query.append("RETURN {question: q.text, answer: collect(answers)}");

        return this.getQueryResult(query.toString());
    }

    /**
     * Gets the prevention strategy.
     *
     * @param id The id of the chain analysis.
     * @return A JsonNode containing the information about the prevention strategy.
     */
    public JsonNode getPreventionStrategy(long id) {
        StringBuilder query = new StringBuilder("MATCH (m:").append(ChainAnalysisMetaModel.TYPE).append(")");
        query.append("-[:STEP]->(s)-[:QUESTION]->(q)-[:ANSWER]->(a)\n");
        query.append("WHERE id(m) = ").append(id).append(" AND s.text = 'preventionStrategy'\n");
        query.append("RETURN {question: q.text, answer: a.text}");

        return this.getQueryResult(query.toString());
    }

    /**
     * Executes and returns the data of the given query. It is assumed that the query returns just one column,
     * as that is the only place it will look for data.
     *
     * @param query The query that needs to be executed.
     * @return A JsonNode containing the data of the query.
     */
    private JsonNode getQueryResult(String query) {
        // Get the first column of the first result (there should be only one of either).
        JsonNode result = GraphDBService.execute(query).get("data").get(0);
        if (result != null) {
            return result.get(0);
        }
        return null;
    }

}
