package models.graphobjects;

import org.neo4j.graphdb.Label;

public enum NodeType implements Label {
    QUESTION,
    ANSWER,
    WORD,
    WORD_CATEGORY
}
