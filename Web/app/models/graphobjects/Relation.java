package models.graphobjects;

import org.neo4j.graphdb.RelationshipType;

public enum Relation implements RelationshipType {
    WORD_OF_CATEGORY,
    ANSWER_OF_QUESTION,
    WORD_CATEGORY
}
