package models;

import services.LogService;

import java.util.ArrayList;

public class NormalisedText {
    private String string;
    private WordCategory mainCategory;

    public NormalisedText(String string) {
        LogService.getInstance().info("Nieuwe zin aanmaken: " + string);
        setString(string);
    }

    @Override
    public String toString() {
        return string;
    }

    public ArrayList<Word> getVerbs(WordCategory category) {
        return getWords("verb", null, category);
    }

    public ArrayList<Word> getNouns(WordCategory category) {
        return getWords("noun", null, category);
    }

    public ArrayList<Word> getNames(WordCategory category) {
        return getWords("name", null, category);
    }

    public WordCategory getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(WordCategory mainCategory) {
        this.mainCategory = mainCategory;
    }

    private ArrayList<Word> getWords(String type, ArrayList<Word> words, WordCategory category) {
        if (words == null) {
            words = new ArrayList<>();
        }
        if (category == null) {
            return words;
        }

        for (Word word : category.getWords()) {
            if (word.getPos().equals(type)) {
                words.add(word);
            }
        }

        for (WordCategory item : category.getCategories()) {
            words = getWords(type, words, item);
        }

        return words;
    }

    private void setString(String string) {
        this.string = string;
    }
}
