package models;

import java.util.ArrayList;
import java.util.List;

public class Word {
    private String root;  // stam
    private String pos;  // Part of speech
    private String rel;  // soort woord
    private long id; // optionele id
    private List<String> synonyms;

    public Word(String root, String pos, String rel) {
        this.root = root.toLowerCase();
        this.pos = pos.toLowerCase();
        this.rel = rel.toLowerCase();
        this.synonyms = new ArrayList<>();
    }

    public Word(String root, String pos, String rel, long id) {
        this(root, pos, rel);
        this.setId(id);
    }

    public void addSynonym(String synonym) {
        this.synonyms.add(synonym);
    }

    public List<String> getSynonyms() {
        return synonyms;
    }

    public String getRoot() {
        return root;
    }

    public String getPos() {
        return pos;
    }

    public String getRel() {
        return rel;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Word word = (Word) o;

        if (!root.equals(word.root)) return false;
        if (!pos.equals(word.pos)) return false;
        return rel.equals(word.rel);

    }

    @Override
    public int hashCode() {
        int result = root.hashCode();
        result = 31 * result + pos.hashCode();
        result = 31 * result + rel.hashCode();
        return result;
    }

    public String toString() {
        return String.format("WORD: %s, REL: %s, ROOT: %s", getRel(), getPos(), getRoot());
    }
}
