package models;

public class Product{
	private String product; 
	private String unity; 
	private String kcal; 
	private String kJ;
	private int id; 
   
	public Product(String product, String unity, String kcal, String kJ, int id){
		this.setProduct(product);
		this.setUnity(unity);
		this.setKcal(kcal);
		this.setKj(kJ);
		this.setId(id);
	}
	
	public String getProduct(){
		return product;
	}
	
	public void setProduct(String product){
		this.product= product;
	}
	
	public String getUnity(){
		return unity;
	}
	
	public void setUnity(String unity){
		this.unity = unity;
	}
	
	public String getKcal(){
		return kcal; 
	}
	
	public void setKcal(String kcal){
		this.kcal=kcal; 
	}	
	
	public String getKj(){
		return kJ;
	}
	
	public void setKj(String kJ){
		this.kJ=kJ;
	}
	
	public int getId(){
		return id;
	}
	public void setId(int id){
		this.id=id;
	}
	
}

