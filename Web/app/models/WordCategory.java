package models;

import services.LogService;

import java.util.ArrayList;
import java.util.Objects;

public class WordCategory {
    private String name;
    private ArrayList<WordCategory> categories;
    private WordCategory parent;
    private ArrayList<Word> words;

    public WordCategory(WordCategory parent, String name) {
        this.setName(name);
        this.setParent(parent);
        this.categories = new ArrayList<>();
        this.words = new ArrayList<>();
        LogService.getInstance().info("Woordcategorie " + name + " met parent " + (parent == null ? "-" : parent.getName()) + " aanmaken.");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Voeg woord toe aan object
     *
     * @param pos  Part of speech
     * @param rel  Soort woord
     * @param root Stamvorm
     */
    public void addWord(String pos, String rel, String root) {
        if (root.equals("")) return; // We willen geen lege woorden toevoegen

        this.words.add(new Word(root, pos, rel));
        LogService.getInstance().info(
            String.format("Woord toegevoegd aan %s: WORD: %s, REL: %s, ROOT: %s", name, pos, rel, root)
        );
    }

    public ArrayList<Word> getWords() {
        return words;
    }

    public WordCategory getParent() {
        return parent;
    }

    public void setParent(WordCategory parent) {
        this.parent = parent;
    }

    public ArrayList<WordCategory> getCategories() {
        return categories;
    }

    public boolean hasCategories() {
        return !getCategories().isEmpty();
    }

    public WordCategory addCategory(String category) {
        WordCategory newCategory = new WordCategory(this, category);
        this.categories.add(newCategory);
        return newCategory;
    }
}
