package models;


import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;

public class Neo4jQuery {
    public String query;

    public Neo4jQuery(String q) {
        this.query = q;
    }

    public JsonNode toJson() {
        return Json.toJson(this);
    }
}
