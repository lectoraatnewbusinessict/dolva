package models.chain;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.BaseModel;
import play.libs.Json;
import services.ChainAnalysisService;

/**
 * Base model of chain analysis meta information.
 */
public class ChainAnalysisMetaModel extends BaseModel {

    public static final String TYPE = "ChainAnalysisMetadata";

    public ChainAnalysisMetaModel() {
    }

    public ChainAnalysisMetaModel(String name, long analysisDate, long behaviorDate) {
        this.addProperty("name", name);
        this.addProperty("analysisDate", String.valueOf(analysisDate));
        this.addProperty("behaviorDate", String.valueOf(behaviorDate));
    }

    @Override
    public String getType() {
        return TYPE;
    }

    public JsonNode toJSON(ChainAnalysisService service) {
        ObjectNode result = Json.newObject();
        result.put("id", this.getId());
        result.put("name", this.getProperties().get("name"));
        result.put("analysisDate", Long.valueOf(this.getProperties().get("analysisDate")));
        result.put("behaviorDate", Long.valueOf(this.getProperties().get("behaviorDate")));
        result.put("problemBehavior", service.getProblemBehavior(this.getId()));
        result.put("provocation", service.getProvocation(this.getId()));
        result.put("vulnerabilityFactors", service.getVulnerabilityFactors(this.getId()));
        result.put("eventChain", service.getEventChain(this.getId()));
        result.put("consequences", service.getConsequences(this.getId()));
        result.put("preventionStrategy", service.getPreventionStrategy(this.getId()));

        return result;
    }

}
