package models.chain;

import models.BaseModel;

/**
 * Base model of a single chain analysis link.
 */
public class ChainAnalysisModel extends BaseModel {

    public static final String TYPE = "ChainAnalysis";

    @Override
    public String getType() {
        return TYPE;
    }

}
