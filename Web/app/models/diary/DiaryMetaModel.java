package models.diary;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.BaseModel;
import play.libs.Json;
import services.DiaryService;

/**
 * Base model for diary meta information.
 */
public class DiaryMetaModel extends BaseModel {

    public static final String TYPE = "DiaryMetadata";

    public DiaryMetaModel() {
    }

    public DiaryMetaModel(String name, long date) {
        this.addProperty("name", name);
        this.addProperty("date", String.valueOf(date));
    }

    @Override
    public String getType() {
        return TYPE;
    }

    public JsonNode toJSON(DiaryService service) {
        ObjectNode result = Json.newObject();
        result.put("id", this.getId());
        result.put("name", this.getProperties().get("name"));
        result.put("date", Long.valueOf(this.getProperties().get("date")));
        result.put("comments", service.getComments(this.getId()));
        result.put("feelings", service.getFeelings(this.getId()));
        result.put("obstacles", service.getObstacles(this.getId()));
        result.put("irrelevantBehavior", service.getIrrelevantBehavior(this.getId()));

        return result;
    }

}
