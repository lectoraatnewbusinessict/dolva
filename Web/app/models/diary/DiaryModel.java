package models.diary;

import models.BaseModel;

/**
 * Base model of diary entry.
 */
public class DiaryModel extends BaseModel {

    @Override
    public String getType() {
            return "Diary";
        }

}
