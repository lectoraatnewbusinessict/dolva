package models;

public class Meal{
   private String date; 
   private String meal; 
   private String calories;
//   private int id; 
   
   public Meal(String date, String meal, String calories){
	  this.setDate(date);
	  this.setMeal(meal);
	  this.setCalories(calories);
//	  this.setId(id);
   }
   
   public String getDate(){
	  return date;
   }
   
   public void setDate(String date){
	   this.date = date; 
   }
   
   public String getMeal(){
	   return meal; 
   }
   
   public void setMeal(String meal){
	   this.meal=meal;
   }
      
   public String getCalories(){
	   return calories;
   }
   
   public void setCalories(String calories){
	   this.calories = calories; 
   } 
//   
//   public int getId() {
//	   return id;
//   }
//   
//   public void setId(int id){
//	   this.id=id; 
//   }
}

