package models;

public class Account {

	private String name;
	private String surname;
	private String role;
	private String password;
	private String login;
	private String date;
	private String username;
	
	public Account(String name, String surname, String date, String role, String username) {
		this.setName(name);
		this.setSurname(surname);
		this.setDate(date);
		//this.setPassword(password);
		//this.setLogin(login);
		this.setUsername(username);
		this.setRole(role);
	}

	///////////////////////////////////////////////////////

	
	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	///////////////////////////////////////////////////////
	
	public String getName(){
		return name;
	}
	
	public String getSurname(){
		return surname;
	}
	
	public String getRole(){
		return role;
	}
	
	public String getLogin(){
		return login;
	}
	
	public String getPassword(){
		return password;
	}
	
	public String getDate(){
		return date;
	}
	
	public String getUsername(){
		return username;
	}
}