package models;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import services.GraphDBService;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Base model for Neo4J nodes. Can be used to save new nodes to the graph database.
 */
public abstract class BaseModel {

    private final Map<String, String> properties = new HashMap<>();
    private int id;

    /**
     * Saves Neo4J node data and stores its internal id.
     */
    public void create() {
        StringBuilder builder = new StringBuilder("CREATE (model:");
        builder.append(this.getType());
        if (this.getProperties().size() > 0) {
            builder.append("{");
            Iterator<String> keyIterator = this.getProperties().keySet().iterator();
            while(keyIterator.hasNext()) {
                String key = keyIterator.next();
                String value = this.getProperties().get(key);
                builder.append(key);
                builder.append(": ");
                builder.append('"');
                builder.append(value);
                builder.append('"');
                if (keyIterator.hasNext()) {
                    builder.append(", ");
                }
            }
            builder.append("}");
        }
        builder.append(") RETURN model");

        JsonNode node = GraphDBService.execute(builder.toString());
        this.id = node.findPath("id").asInt();
    }

    /**
     * Adds a property to the node.
     *
     * @param key   the key of the property
     * @param value the value of the property
     */
    public void addProperty(String key, String value) {
        this.properties.put(key, value);
    }

    /**
     * Adds a list of properties to the nodes.
     *
     * @param properties A list of properties.
     */
    public void addProperties(Map<String, String> properties) {
        this.properties.putAll(properties);
    }

    /**
     * Maps a relation with another node.
     *
     * @param otherId      the id of the other node.
     * @param relationName the description of the relation.
     */
    public void relateTo(int otherId, String relationName) {
        // Find the nodes by internal id and create a relationship between them with the specified name.
        GraphDBService.execute("MATCH (n), (m) WHERE ID(n) = " + this.id + " AND ID(m) = "
                + otherId + " CREATE (n)-[:" + relationName + "]->(m)");
    }

    /**
     * Deletes all related nodes recursively, but only in one direction, starting from this node. The node itself is also deleted.
     */
    public void deleteAllFromHere() {
        GraphDBService.execute(String.format("MATCH (n)-[*]->(m) WHERE id(n) = %d DETACH DELETE n, m", this.getId()));
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public abstract String getType();

    public Map<String, String> getProperties() {
        return this.properties;
    }

    /**
     * Returns a JSON representation of the model. If not overridden, this method simply returns an empty JSON object.
     *
     * @return A JsonNode representing the model.
     */
    public JsonNode toJSON() {
        return Json.toJson(this);
    }

}
