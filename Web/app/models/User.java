package models;

public class User {
	String name;
	String surname;
	String username;
	String password;
	String date;
	String function;
	int id;

	public User(String name, String surname, String username, String password, String date,
			String function, int id) {
		this.setName(name);
		this.setSurname(surname);
		this.setUserName(username);
		this.setPassword(password);
		this.setDate(date);
		this.setFunction(function);
		this.setId(id);
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getSurname(){
		return surname;
	}
	
	public void setSurname(String surname){
		this.surname = surname;
	}

	public String getUserName() {
		return username;
	}
	
	public void setUserName(String username){
		this.username = username;
	}

	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password){
		this.password = password;
	}

	public String getDate() {
		return date;
	}
	
	public void setDate(String date){
		this.date = date;
	}

	public String getFunction() {
		return function;
	}
	
	public void setFunction(String function){
		this.function = function;
	}
		
	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
}
