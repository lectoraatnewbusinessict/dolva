package models;

public class Question {
    private String question;
    private long id;

    public Question(String question) {
        this.setQuestion(question);
    }

    public Question(String question, long id) {
        this(question);
        this.setId(id);
    }

    public String getQuestion() {
        return question;
    }

    private void setQuestion(String question) {
        this.question = question;
    }

    public long getId() {
        return id;
    }

    private void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return getQuestion();
    }
}
