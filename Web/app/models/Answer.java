package models;

public class Answer {
    private String answer;
    private String category;
    private int id;

    public Answer(String answer, String category, int id) {
        this.setAnswer(answer);
        this.setCategory(category);
        this.setId(id);
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
