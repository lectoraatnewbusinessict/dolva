package models;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class Settings {

    private static Settings instance;

    private Config conf = ConfigFactory.load(); // allows to use *.conf file on CLASSPATH for settings, which can be overridden by passing arguments to activator (see Play documentation)
   
   // {System.out.println(conf.getString("test"));}
    public static Settings getInstance() {
        if (instance == null) {
            instance = new Settings();
        }
        return instance;
    }

    public String getALPINO_PATH() {
        return conf.getString("Alpino.Path"); //isLinux() ? ALPINO_PATH_LINUX : ALPINO_PATH_WINDOWS;
    }

    public String getNEO4J_AUTH() {
        return conf.getString("Neo4J.Auth"); // "bmVvNGo6YWRtaW4xMjM=";
    }

    public String getNEO4J_SERVER() {
        return conf.getString("Neo4J.Server"); // http://185.66.250.184:7474";
    }

}
